package system;

import java.awt.AWTException;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Robot;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

import game.KeyPoint;

public class ScreenSelection {

	private static ScreenSelection ss;

	private int screenDigit = -1, yStartingValue;
	GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice[] gs = ge.getScreenDevices();


	private ScreenSelection() {

	}

	public static ScreenSelection getInstance() {
		if (ss == null) {
			ss = new ScreenSelection();
		}
		return ss;
	}

	public BufferedImage takeScreenshot(int screenNumber) {
		BufferedImage image;
		try {
			image = new Robot().createScreenCapture(gs[screenNumber].getDefaultConfiguration().getBounds());
			return image;
		} catch (HeadlessException e) {

			e.printStackTrace();
		} catch (AWTException e) {

			e.printStackTrace();
		}
		return null;
	}

	public int getScreenNumber() {
        return gs.length;
	}

	public void setScreenDigit(int screenDigit) {
		this.screenDigit = screenDigit;
	}

	public int getScreenDigit() {
		return screenDigit;
	}

	public KeyPoint getKeyPoint() {
        return new KeyPoint(gs[screenDigit].getDefaultConfiguration().getBounds().x, gs[screenDigit].getDefaultConfiguration().getBounds().y);

	}

	public int getScreenWidth() {
		return (int) gs[screenDigit].getDefaultConfiguration().getBounds().getWidth();
	}

	public int getScreenHeight() {
		return (int) gs[screenDigit].getDefaultConfiguration().getBounds().getHeight();
	}
	
	public String getScreenValues() {
		return ScreenSelection.getInstance().getScreenWidth() + "x" + ScreenSelection.getInstance().getScreenHeight();
	}

	public ImageIcon resizeToScreenSize(String hashKey) {
		
		ImageIcon image = TextureHandler.getImage(hashKey);
		
		int screenHeight = ScreenSelection.getInstance().getScreenHeight() ;
		int screenWidth = ScreenSelection.getInstance().getScreenWidth();
		int imageWidth = image.getIconWidth();
		
		double teiler = screenWidth*100000/imageWidth;
		teiler = teiler/100000;
		
		int newIconWidth = (int)(Math.round(image.getIconWidth() * teiler));
		int newIconHeight = (int)(Math.round(image.getIconHeight() * teiler));

		yStartingValue = (screenHeight - newIconHeight) / 2;
		
		image = TextureHandler.resizeImage(image, newIconWidth, newIconHeight);
		return image;
	}
	
	public int getYStartingValue() {
		return yStartingValue;
	}
}
