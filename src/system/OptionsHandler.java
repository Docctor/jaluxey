/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package system;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Properties;

public class OptionsHandler {
	
	private Properties config;
	private File file;
	
	public OptionsHandler() {
		file = new File("./config.properties");
		config = new Properties();
	}
	
	public void saveDefaultConfig() throws IOException {
		if (file.exists()) return;
		
		config.setProperty("game.teamColor", "BLUE");
		config.setProperty("game.selectedArena", "standard");
		
		config.setProperty("sound.volume", "100");
		
		Writer os = new FileWriter(file);
		config.store(os, null);
	}
	
	public void loadConfig() throws IOException {
		FileReader reader = new FileReader(file);
		config.load(reader);
	}
	
	public Properties getConfig() {
		return config;
	}
	
	public String getOption(String key) {
		return config.getProperty(key);
	}
	
	public void setOption(String key, String value) {
		config.setProperty(key, value);
	}
	
	public void saveOptions() throws IOException {
		Writer os = new FileWriter(file);
		config.store(os, null);
	}
}
