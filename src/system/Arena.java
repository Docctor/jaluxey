/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package system;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import git.bMINUSdocctor.ioStream.IOMode;
import git.bMINUSdocctor.ioStream.IOStream;
import git.bMINUSdocctor.ioStream.UnkownIOMode;
import git.bMINUSdocctor.ioStream.WrongIOMode;
import model.Sun;
import team.TeamColor;

public class Arena {
	
	public static Map<String, Arena> maps;
	private ArrayList<Sun> suns;
	
	public Arena(ArrayList<Sun> suns) {
		maps = new HashMap<String, Arena>();
		this.suns = suns;
	}
	
	public ArrayList<Sun> getSuns() {
		return suns;
	}
	
	//STATIC METHODS
	
	public static void loadArenas() throws IOException, UnkownIOMode, WrongIOMode {
		File file = new File("./maps/");
		File[] files = file.listFiles();
		if (files == null) return;
		
		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory()) continue;
			if (!files[i].getPath().endsWith(".arena")) continue;
			
			ArrayList<Sun> sun = new ArrayList<Sun>();
			File arena = files[i];
			IOStream stream = new IOStream(arena, IOMode.READ);
			
			String line;
			while ((line = stream.readNextLine()) != null) {
				String infos[] = line.split(",");
				float x = Float.parseFloat(infos[0]);
				float y = Float.parseFloat(infos[1]);
				int level = 0;
				int levelCap = Integer.parseInt(infos[2]);
				TeamColor team = TeamColor.valueOf(infos[3]);
				sun.add(new Sun(x, y, team, level, levelCap));
			}
			Arena map = new Arena(sun);
			maps.put(arena.getName().replace(".arena", ""), map);
		}
		
	}
	
	public static Arena getArena(String key) {
		return maps.get(key);
	}
	
	public static String[] getArenaNames() {
		Object[] keysRaw = maps.keySet().toArray();
		String[] keys = Arrays.copyOf(keysRaw, keysRaw.length, String[].class);
		return keys;
	}
	
}
