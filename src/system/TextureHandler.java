package system;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import main.Main;
import view.GameFrameGUI;

public class TextureHandler {
	private static Map<String, ImageIcon> images;
	
	private static File scrollStageFolder = new File("./resources/ScrollStages");
	
	private static ArrayList<File> scaledImageFolder = new ArrayList<File>();
	
	private static ArrayList<String> scaledImageTempKey = new ArrayList<String>();
	
	private static ArrayList<Font> costumFonts = new ArrayList<Font>();
	
	private static ArrayList<BufferedImage> scaledBufferedImages = new ArrayList<BufferedImage>();
	
	private static int numberScrollStages;

	/**
	 * Loads all images from the resources folder
	 */
	public static void loadImages() {
		images = new HashMap<String, ImageIcon>();
		
		//Images
		readOriginalResources();	
	}
	
	public static void loadScrollStages() {
		//Scaled Images
		TextureHandler.createAndFillScreenResolutionFolder();
		TextureHandler.readImageScrollStages();
	}
	
	public static void loadFonts() {
		//Fonts
		readFonts();
	}
	
	public static String[] getKeys() {
		Object[] keys = images.keySet().toArray();
		return Arrays.copyOf(keys, keys.length, String[].class);
	}
	
	/**
	 * Gives an icon back
	 * @param key The key of the specific icon.
	 * @return The icon
	 */
	public static ImageIcon getImage(String hashKey) {
		return images.get(hashKey);
	}
	
	/**
	 * Resizes an icon to an individual size.
	 * @param icon The icon to resize
	 * @param targetWidth  Width of the new icon
	 * @param targetHeight height of the new icon
	 * @return The resized image as icon
	 * java.awt.Image.SCALE_SMOOTH
	 */
	public static ImageIcon getSizedImage(String hashKey, int width, int height) {
		Image texture = images.get(hashKey).getImage();
		Image sized = texture.getScaledInstance(width, height,  Image.SCALE_SMOOTH);
		ImageIcon textureSized = new ImageIcon(sized);
	    return textureSized;
	}
	
	public static ImageIcon resizeImage(ImageIcon texture, int width, int height) {
		Image sized = texture.getImage().getScaledInstance(width, height,  Image.SCALE_FAST);
		ImageIcon textureSized = new ImageIcon(sized);
	    return textureSized;
	}
	
	public static void addImage(String name) {
		images.put("texture."+name, new ImageIcon("./resources/" + name + ".png"));
	}
	
	public static Image toImage(String image) {
		ImageIcon icon = images.get(image);
		return icon.getImage();
	}
	
	public static String getColor(int team) {
		switch (team) {
		case 0:
			return "";
		case 1:
			return "_blue";
		case 2:
			return "_green";
		case 3:
			return "_orange";
		default:
			throw new IllegalArgumentException("Unexpected value: " + team);
		}
	}
	
	
	
	//Image scaling
	public static void setNumberScrollStages() {
		int scrollDifference = Main.getMaximumScroll() - Main.getMinimumScroll();
		numberScrollStages = (int) ((scrollDifference / 0.1) + 1);
	}
	
	public static int getNumberScrollStage() {
//		int scrollStage = (int) ((Main.getScaleX() - Main.getMinimumScroll()) * 10);
		int scrollStage = (int)((Main.getGameFrame().getWorld().getScaleX() - Main.getMinimumScroll()) * 10);
		return scrollStage;
	}
	
	public static BufferedImage convertImageIconToBufferedImage(ImageIcon oldImage) {
		BufferedImage newImage = new BufferedImage(oldImage.getIconWidth(), oldImage.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics graphicsTemp = newImage.createGraphics();
		oldImage.paintIcon(null, graphicsTemp, 0, 0);
		graphicsTemp.dispose();
		return newImage;
	}
	
	public static void createScreenResolutionFolder() {
		if(!scrollStageFolder.exists()) {
			scrollStageFolder.mkdirs();
		}
	}
	
	public static void createScaledImageFolder() {
		for (int i = 0; i < scaledImageTempKey.size(); i++) {
			scaledImageFolder.add(new File (scrollStageFolder.getPath() + "/" + scaledImageTempKey.get(i)));
			if (!scaledImageFolder.get(i).exists()) {
				scaledImageFolder.get(i).mkdirs();
			}
		}
	}

	public static void prepareScaledImages() {
		
		double objectWidth;
		double objectHeight;
		int width;
		int height;
		for (Map.Entry<String, ImageIcon> entry : images.entrySet()) {
			if (entry.getKey().equals("MainMenueTitelIcon") || entry.getKey().equals("PauseScreenBackground") || entry.getKey().equals("GameOptionsAudioMenue") || entry.getKey().equals("GameOptionsGameMenue") || entry.getKey().equals("GameOptionsMenue") || entry.getKey().equals("GamePauseOptionsAudioMenue") || entry.getKey().equals("WIN") || entry.getKey().equals("LOSE")){
				continue;
			}
			scaledImageTempKey.add(entry.getKey());
			ImageIcon unscaledImage = entry.getValue();
			for (float i = Main.getMinimumScroll(); i <= Main.getMaximumScroll(); i += 0.1f) {
				
				i = Math.round(i * 100) / 100.0f;
				
				objectWidth = 1;
				objectHeight = 1;
				
				if (entry.getKey().contains("Orb_Boom")) {
					objectWidth = 0.1;
					objectHeight = 0.1;
				}
				
				width = (int) Math.round((objectWidth * Main.getCellPixelSize() * i));
				height = (int) Math.round((objectHeight * Main.getCellPixelSize() * i));
				
				ImageIcon scaledImage = TextureHandler.resizeImage(unscaledImage, width, height);
				BufferedImage scaledBufferedImage = TextureHandler.convertImageIconToBufferedImage(scaledImage);
				
				scaledBufferedImages.add(scaledBufferedImage);
			}	
		}
	}
	
	public static void fillScreenResolutionFolder() {
		TextureHandler.prepareScaledImages();
		TextureHandler.createScaledImageFolder();
		TextureHandler.setNumberScrollStages();
		
		int oldi = 0;
		int numberImageIcon = 0;
		int numberScrollStage = 0;
		for (int i = 0; i < scaledBufferedImages.size(); i++) {
			
			if (i - oldi == numberScrollStages) {
				oldi = i;
				numberImageIcon++;
				numberScrollStage = 0;
			}
			BufferedImage scaledBufferedImage = scaledBufferedImages.get(i);
			File outputFile = new File(scaledImageFolder.get(numberImageIcon).getPath() + "/" + scaledImageTempKey.get(numberImageIcon) + "_scrollStage" + numberScrollStage + ".png");
			numberScrollStage++;
			if (!outputFile.exists()) {				
				try {					
					ImageIO.write(scaledBufferedImage, "png", outputFile);				
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void createAndFillScreenResolutionFolder() {
		if (!scrollStageFolder.exists()) {
			TextureHandler.createScreenResolutionFolder();
			TextureHandler.fillScreenResolutionFolder();
		}
	}
	
	public static ImageIcon resizeScrollStageByAnimation(String originalKey, int currentFrame) {
		int scrollStage = TextureHandler.getNumberScrollStage();
		ImageIcon resizedCurrentFrame = TextureHandler.getImage(originalKey + currentFrame + "_scrollStage" + scrollStage);
		
		return resizedCurrentFrame;
	}
	
	public static ImageIcon resizeScrollStage(String originalKey) {
		int scrollStage = TextureHandler.getNumberScrollStage();
		ImageIcon resizedCurrentFrame = TextureHandler.getImage(originalKey + "_scrollStage" + scrollStage);
		
		return resizedCurrentFrame;
	}
	
	public static void readImageScrollStages() {
		TextureHandler.setNumberScrollStages();
		ArrayList<String> scrollStageImageKeys = new ArrayList<String>();
		for (String hashKey : images.keySet()) {
			scrollStageImageKeys.add(hashKey);
		}
		for (String hashKey : scrollStageImageKeys) {
			for (int i = 0; i < numberScrollStages; i++) {
				String scrollStageImageFolderPath = scrollStageFolder.getPath() + "/" + hashKey + "/" + hashKey + "_scrollStage" + i + ".png";	
				String scrollStageImageKey = hashKey + "_scrollStage" + i;
				ImageIcon imgTemp = new ImageIcon(scrollStageImageFolderPath);
				images.put(scrollStageImageKey, imgTemp);
			}
		}
	}
	
	//End of Image scaling	
	public static void readOriginalResources() {
		File dir = new File("./resources/Original Resources");
		for (File file : dir.listFiles()) {
			if (!file.isDirectory()) {
				String hashKey = file.getName().replaceFirst("[.][^.]+$", "");
				images.put(hashKey, new ImageIcon(file.getPath()));
			} 
		}
	}
	
	//Fonts
	public static void readFonts() {
		File dir = new File("./resources/Fonts");
		try {
			for (File file : dir.listFiles()) {
				if (!file.isDirectory()) {	
					costumFonts.add(Font.createFont(Font.TRUETYPE_FONT, file));
				}
			}
		} catch (IOException|FontFormatException e) {
		}
	}
	
	public static Font getFont(int index) {
		return costumFonts.get(index);
	}
	
	public static Font getFont(String name) {
		for (Font font : costumFonts) {
			if (font.getName().equals(name)) {
				return font;
			}
		}
		System.out.println(name + "konnte in dieser Liste nicht gefunden werden:");
		getFontList();
		return costumFonts.get(0);
	}
	
	public static void getFontList() {
		for (Font font : costumFonts) {
			System.out.println(font.getName());
		}
	}
	
	public static ImageIcon[] getImageIconArray(String key) {
		ImageIcon[] imageIconArray = new ImageIcon[numberScrollStages];
		for (int i = 0; i < imageIconArray.length; i++) {
			String scrollStageImageFolderPath = scrollStageFolder.getPath() + "/" + key + "/" + key + "_scrollStage" + i + ".png";
			imageIconArray[i] = new ImageIcon(scrollStageImageFolderPath);
		}
		return imageIconArray;
	}
}
