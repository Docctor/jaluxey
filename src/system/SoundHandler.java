/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package system;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import audiocue.AudioCue;

public class SoundHandler {
	
	private static Map<String, AudioCue> sounds;
	
	public static void loadSounds() throws UnsupportedAudioFileException, IOException, IllegalStateException, LineUnavailableException {
		sounds = new HashMap<String, AudioCue>();
		File soundsFolder = new File("./resources/sounds");
		File[] allSounds = soundsFolder.listFiles();
		for (int i = 0; i < allSounds.length; i++) {
			if (!allSounds[i].getName().endsWith(".wav")) continue;
			String id = allSounds[i].getName().replace(".wav", "");
			URL url = allSounds[i].toURI().toURL();
			AudioCue audio = AudioCue.makeStereoCue(url, 50);
			sounds.put(id, audio);
			audio.open();
		}
	}
	
	public static String[] getAllSoundNames() {
		Object[] keysRaw = sounds.keySet().toArray();
		return Arrays.copyOf(keysRaw, keysRaw.length, String[].class);
	}
	
	public static void playSound(String id, int volume) {
		double volumeConverted = volume/100.0;
		sounds.get(id).play(volumeConverted);
	}
}
