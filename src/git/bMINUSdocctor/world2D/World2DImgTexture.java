/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package git.bMINUSdocctor.world2D;

import javax.swing.ImageIcon;

class World2DImgTexture implements World2DTexture {
	private ImageIcon originalTexture;
	private ImageIcon[] texturePreloaded;
	
	World2DImgTexture(ImageIcon originalTexture, ImageIcon[] texturePreloaed) {
		this.originalTexture = originalTexture;
		this.texturePreloaded = texturePreloaed;
		
	}
	
	public ImageIcon getTexture(int scaleLevel) {
		return texturePreloaded[scaleLevel];
	}
	
	public ImageIcon getOriginalTexture() {
		return originalTexture;
	}
}
