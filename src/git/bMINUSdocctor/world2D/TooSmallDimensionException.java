/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package git.bMINUSdocctor.world2D;

@SuppressWarnings("serial")
public class TooSmallDimensionException extends Exception {

	
	public TooSmallDimensionException(String message) {
		super(message);
	}
	
	public TooSmallDimensionException() {
		super();
	}
}
