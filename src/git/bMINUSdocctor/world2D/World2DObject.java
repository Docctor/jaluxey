/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package git.bMINUSdocctor.world2D;

import java.awt.Color;
import java.util.UUID;

import javax.swing.JLabel;

public class World2DObject {
	
	private double x, y, width, height;
	private World2DTexture texture;
	private JLabel object;
	private int layer;
	private UUID uuid;
	
	World2DObject(double x, double y, double width, double height, int layer, boolean opaque) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.layer = layer;
		object = new JLabel();
		object.setBackground(Color.BLACK);
		object.setOpaque(opaque);
		uuid = UUID.randomUUID();
	}
	
	World2DObject(double x, double y, double width, double height, int layer, World2DTexture texture, boolean opaque) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.texture = texture;
		this.layer = layer;
		object = new JLabel();
		object.setOpaque(opaque);
		uuid = UUID.randomUUID();
	}

	/**
	 * Returns the X position of the Object in the world
	 * @return
	 */
	public double getX() {
		return x;
	}

	/**
	 * Sets the X position of the Object in the world
	 * @return
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * Returns the Y position of the Object in the world
	 * @return
	 */
	public double getY() {
		return y;
	}

	/**
	 * Sets the Y position of the Object in the world
	 * @return
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * Returns the width of the Object
	 * @return
	 */
	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	/**
	 * Returns the height of the Object
	 * @return
	 */
	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	World2DTexture getTexture() {
		return texture;
	}
	
	/**
	 * Sets the texture for the object
	 * @param texture
	 */
	public void setTexture(World2DTexture texture) {
		this.texture = texture;
	}

	JLabel getObject() {
		return object;
	}

	void setObject(JLabel object) {
		this.object = object;
	}
	
	/**
	 * Checks if the object is visible
	 * @return true - when the object is visible
	 */
	public boolean isVisible() {
		return object.isVisible();
	}
	
	/**
	 * Sets the visibility of the object in the world
	 * @param value
	 */
	public void setVisible(boolean value) {
		object.setVisible(value);
	}
	
	/**
	 * Return the layer of the object
	 * @return
	 */
	public int getLayer() {
		return layer;
	}
	
	public void setColor(Color color) {
		object.setBackground(color);
	}
	
	public UUID getUniqueID() {
		return uuid;
	}
}
