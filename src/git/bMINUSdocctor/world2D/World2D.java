/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package git.bMINUSdocctor.world2D;

import java.awt.Color;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import view.Auswahlrahmen;


public class World2D {
	
	private World2DDebugScreen debugScreen;
	
	private Map<String, World2DObject> objects;
	
	private JLayeredPane world;
	
	private JPanel offScreen;
	private boolean showOffScreen;
	
	private World2DObject[] worldBorder;
	
//	private Auswahlrahmen auswahlrahmen;
	
	private World2DObject[] verticalLines;
	private World2DObject[] horizontalLines;
	private boolean showDebugGrid;
	
	private int panelWidth, panelScreenX, panelScreenY, panelHeight, cellPixelSize, maxScale, fps, numberScaleStages;
	private double gridWidth, gridHeight, worldOffSetX, worldOffSetY;
	private float scaleX, scaleY, offSetX, offSetY;
	
	private long lastCall;
	
	/**
	 * The World2D API can be only used on a JPanel with no Layout or added to the contentPane
	 * @param panelScreenX The x position of the panel on your screen (applied on top left corner)
	 * @param panelScreenY The y position of the panel on your screen (applied on top left corner)
	 * @param panelWidth The width of the grid in pixel
	 * @param panelHeight The height of the grid in pixel
	 * @param gridWidth The width of the grid (No pixel size; It is a double, but you can only set full cells)
	 * @param gridHeight The height of the grid (No pixel size)
	 * @param cellPixelSize The pixel size of each cell in the grid (Must be 20 or greater)
	 * @param maxScale The maximum of how much you can zoom in
	 * @param zoomIntensity The intensity of each scroll unit. (Only 1 digit after the point is allowed)
	 * @param offScreen The pause screen
	 * @throws TooSmallDimensionException 
	 */
	public World2D(int panelScreenX, int panelScreenY, int panelWidth, int panelHeight, int gridWidth, int gridHeight, int cellPixelSize, int maxScale, double zoomIntensity, JPanel offScreen) throws TooSmallDimensionException {
		if (panelWidth <= 0 || panelHeight <= 0 || gridWidth <= 0 || gridHeight <= 0 || cellPixelSize < 20 || maxScale <= 0 || zoomIntensity <= 0)
			throw new TooSmallDimensionException();
		this.panelScreenX = panelScreenX;
		this.panelScreenY = panelScreenY;
		this.panelHeight = panelHeight;
		this.panelWidth = panelWidth;
		this.gridWidth = gridWidth;
		this.gridHeight = gridHeight;
		this.cellPixelSize = cellPixelSize;
		this.maxScale = maxScale;
		
		numberScaleStages = (int) Math.round(((maxScale-1)/zoomIntensity)*100)/100;
		
		//OffSet for the world in screenSpace
		worldOffSetY = (panelHeight-gridHeight*cellPixelSize)/2;
		worldOffSetX = (panelWidth-gridWidth*cellPixelSize)/2;
		
		//Creates the world
		world = new JLayeredPane();
		world.setBackground(Color.BLACK);
		world.setLayout(null);
		world.setOpaque(true);
		world.setBounds(panelScreenX, panelScreenY, panelWidth, panelHeight);
		
		objects = new HashMap<String, World2DObject>();
		
		//Component for the Menu screen (First layer)
		if (offScreen != null) {
			this.offScreen = offScreen;
			offScreen.setBounds(0 , 0, panelWidth, panelHeight);
			offScreen.setVisible(false);
			world.add(offScreen, 0);
		}
		
		//ALL Components for the DebugScreen (Second layer)
		debugScreen = new World2DDebugScreen(panelHeight);
		world.add(debugScreen.getlCameraX(), 0);
		world.add(debugScreen.getlCameraY(), 0);
		world.add(debugScreen.getlFps(), 0);
		world.add(debugScreen.getlOffSetX(), 0);
		world.add(debugScreen.getlOffSetY(), 0);
		world.add(debugScreen.getlScaleX(), 0);
		world.add(debugScreen.getlScaleY(), 0);
		
//		//All Components for the world Border (Third Layer)
//		worldBorder = new World2DObject[4];
//		worldBorder[0] = new World2DObject(this.gridWidth/2, -40.05, this.gridWidth*80, 80, 0, true);
//		worldBorder[1] = new World2DObject(this.gridWidth/2, this.gridHeight+40.05, this.gridWidth*80, 80, 0, true);
//		worldBorder[2] = new World2DObject(-40.05, this.gridHeight/2, 80, this.gridHeight*80, 0, true);
//		worldBorder[3] = new World2DObject(this.gridWidth+40.05, this.gridHeight/2, 80, this.gridHeight*80, 0, true);
//		for (World2DObject object: worldBorder) {
//			object.getObject().setBackground(Color.BLACK);
//			world.add(object.getObject(), -1);
//		}
		
//		auswahlrahmen = new Auswahlrahmen();
//		world.add(auswahlrahmen, 0);
		
		//All Components for the debug grid (Fourth layer)
		horizontalLines = new World2DObject[gridHeight+1];
		verticalLines = new World2DObject[gridWidth+1];
		showDebugGrid = false;
		//LEFT TO RIGHT ---
		for (int i = 0; i < gridHeight+1; i++) {
			horizontalLines[i] = new World2DObject(this.gridWidth/2.0, i, this.gridWidth+0.1, 0.1, 0, true);
			horizontalLines[i].getObject().setBackground(new Color(255, 0, 0, 80));
			world.add(horizontalLines[i].getObject());
		}
		//TOP TO BOTTOM |
		for (int i = 0; i < gridWidth+1; i++) {
			verticalLines[i] = new World2DObject(i, this.gridHeight/2.0, 0.1, this.gridHeight+0.1, 0, true);
			verticalLines[i].getObject().setBackground(new Color(255, 0, 0, 80));
			world.add(verticalLines[i].getObject());
		}
		
		
	}
	
	//METHODS FOR RENDERING
	/**
	 * Renders the world on the JPanel
	 * The JPanel layout will be set to absolute, if it isn't already
	 * @param panel The panel to put in
	 */
	public void displayOn(JPanel panel) {
		if (panel.getLayout() != null)
			panel.setLayout(null);
		panel.add(world);
	}
	
	public void removeFrom(JPanel panel) {
		panel.remove(world);
	}
	
	/**
	 * Renders the JPanel
	 * @param fps For debugScreen
	 * @param offSetX For debugScreen
	 * @param offSetY For debugScreen
	 * @param scaleX For debugScreen
	 * @param scaleY For debugScreen
	 */
	public void render() {
		lastCall = System.currentTimeMillis();
		
		//OFFSCREEN
		if (showOffScreen) {
			offScreen.setVisible(true);
		} else {
			offScreen.setVisible(false);
		}
		
		//DEBUGSCREEN GRID
		if (showDebugGrid && debugScreen.isVisible()) {
			for (World2DObject object: verticalLines) {
				object.getObject().setVisible(true);
				setScreenPosition(object);
			}
			for (World2DObject object: horizontalLines) {
				object.getObject().setVisible(true);
				setScreenPosition(object);
			}
		} else {
			for (World2DObject object: verticalLines) {
				object.getObject().setVisible(false);
			}
			for (World2DObject object: horizontalLines) {
				object.getObject().setVisible(false);
			}
		}
		
		//DEBUGSCREEN
		if ((isDebugScreenVisible()) && (!showOffScreen)) {
			debugScreen.getlFps().setVisible(true);
			debugScreen.getlOffSetX().setVisible(true);
			debugScreen.getlOffSetY().setVisible(true);
			debugScreen.getlScaleX().setVisible(true);
			debugScreen.getlScaleY().setVisible(true);
			
			debugScreen.getlFps().setText("FPS: " + this.fps);
			debugScreen.getlOffSetX().setText("OffSetX: " + this.offSetX);
			debugScreen.getlOffSetY().setText("OffSetY: " + this.offSetY);
			debugScreen.getlScaleX().setText("ScaleX: " + this.scaleX);
			debugScreen.getlScaleY().setText("ScaleY: " + this.scaleY);
		} else {
			debugScreen.getlFps().setVisible(false);
			debugScreen.getlOffSetX().setVisible(false);
			debugScreen.getlOffSetY().setVisible(false);
			debugScreen.getlScaleX().setVisible(false);
			debugScreen.getlScaleY().setVisible(false);
		}
		
		
//		//WORLDBORDER
//		for (World2DObject object: worldBorder) {
//			setScreenPosition(object);
//		}
		
		//OBJECTS
		Set<String> keys = objects.keySet();
		for (String key: keys) {
			World2DObject object = objects.get(key);
			if (object.isVisible())
				setScreenPosition(object);
		}
		world.repaint();
	}
	
	//Part of the method render
	private void setScreenPosition(World2DObject object) {
		JLabel label = object.getObject();
		double width = widthToScreen(object.getWidth());
		double height = heightToScreen(object.getHeight());
		int x = worldToScreenX(object.getX(), width);
		int y = worldToScreenY(object.getY(), height);
		label.setBounds(x, y, (int) width, (int) height);
		if (object.getTexture() != null) {
			if (object.getTexture() instanceof World2DImgTexture) {
				World2DImgTexture texture = (World2DImgTexture) object.getTexture();
				label.setIcon(texture.getTexture((int) Math.round((scaleX - 1) * 10)));
			} else {
				World2DImgSequenceTexture textureSequence = (World2DImgSequenceTexture) object.getTexture();
				if (lastCall-textureSequence.getLastFrameTime() >= textureSequence.getNextFrameTime()) {
					
				}
			}
		}
	}
	
	//SETTERS AND GETTERS FOR POSITION, SCALING AND DEBUG INFOS
	int getFps() {
		return fps;
	}

	public void setFps(int fps) {
		this.fps = fps;
	}

	public double getOffSetX() {
		return offSetX;
	}

	public void setOffSetX(float offSetX) {
		this.offSetX = offSetX;
	}

	public double getOffSetY() {
		return offSetY;
	}

	public void setOffSetY(float offSetY) {
		this.offSetY = offSetY;
	}

	public float getScaleX() {
		return scaleX;
	}

	public void setScaleX(float scaleX) {
		this.scaleX = scaleX;
	}

	public float getScaleY() {
		return scaleY;
	}

	public void setScaleY(float scaleY) {
		this.scaleY = scaleY;
	}
	
//	//AUSWAHLRAHMEN
//	public boolean isAuswahlrahmenVisible() {
//		return auswahlrahmen.isVisible();
//	}
//	
//	public void setAuswahlrahmenVisibility(boolean value) {
//		auswahlrahmen.setVisible(value);
//	}
//	
//	public void setAuswahlRahmenPos(int x, int y, int width, int height) {
//		auswahlrahmen.setBounds(x, y, width, height);
//	}
	
	public List<World2DObject> getObjectsInAuswahlrahmen() {
		World2DObject allObjects[] = getAllObjects();
		List<World2DObject> objects = new ArrayList<World2DObject>();
		for (int i = 0; i < allObjects.length; i++) {
			World2DObject object = allObjects[i];
			double width = widthToScreen(object.getWidth());
			double height = heightToScreen(object.getHeight());
			int x = worldToScreenX(object.getX(), width);
			int y = worldToScreenY(object.getY(), height);
			
//			if (x > auswahlrahmen.getX() && y > auswahlrahmen.getY() && x < (auswahlrahmen.getWidth()+auswahlrahmen.getX()) && y < (auswahlrahmen.getHeight()+auswahlrahmen.getY())) objects.add(object);
			
		}
		return objects;
	}
	//END OF METHODS FOR RENDERING
	
	
	
	
	
	//METHODS TO TRANSLATE FROM WORLDSPACE TO SCREENSPACE
	public int worldToScreenX(double worldX, double width) {
		return (int) (   (worldX*cellPixelSize + worldOffSetX - offSetX) * scaleX -(width/2)   );
	}
	
	public int widthToScreen(double width) {
		return (int) (width * cellPixelSize * scaleX);
	}
	
	public double widthToWorld(int width) {
		return ((double)(width))/cellPixelSize/scaleX;
	}

	public int worldToScreenY(double worldY, double height) {
		return (int) (   (worldY*cellPixelSize + worldOffSetY - offSetY) * scaleY -(height/2)   );
	}
	
	public int heightToScreen(double height) {
		return (int) (height * cellPixelSize * scaleY);
	}
	
	public double heightToWorld(int height) {
		return ((double)(height))/cellPixelSize/scaleY;
	}
	
	public double screenToWorldX(float screenX, double width) {
		return (offSetX*scaleX+screenX-scaleX*worldOffSetX+width/2)/(cellPixelSize*scaleX);
	}

	public double screenToWorldY(float screenY, double height) {
		return (offSetY*scaleY+screenY-scaleX*worldOffSetY+height/2)/(cellPixelSize*scaleY);
	}
	
	public double screenToRawWorldX(float screenX, double width) {
		return (offSetX*scaleX+screenX-scaleX*worldOffSetX+width*2)/(cellPixelSize*scaleX);
	}

	public double screenToRawWorldY(float screenY, double height) {
		return (offSetY*scaleY+screenY-scaleX*worldOffSetY+height*2)/(cellPixelSize*scaleY);
	}
	//END OF WORLDSPACE, SCREENSPACE TRANSLATION
	
	
	
	
	
	//METHODS FOR WORLD2D OBJECTS
	/**
	 * Puts an World2D object into the World
	 * @param key The ID of the World2D Object
	 * @param object The World2D Object
	 */
	public void putObjectInWorld(World2DObject object) {
		objects.put(object.getUniqueID().toString().replace("-", ""), object);
		world.add(object.getObject(), object.getLayer()+11+getDebugGridLineNumber());
	}
	
	/**
	 * Removes an objects from the world
	 * @param key Name of the object
	 */
	public void removeObjectFromWorld(String key) {
		World2DObject object = objects.get(key);
		world.remove(object.getObject());
		objects.remove(key);
	}
	
	
	/**
	 * Creates a new World2D Object
	 * @param x The x coordinate at which the object should be in the world 
	 * @param y The y coordinate at which the object should be in the world 
	 * @param width The width of the object. A width of 1 corresponds to the width of a cell 
	 * @param height The height of the object. A height of 1 corresponds to the height of a cell
	 * @param layer 0 Highest, above lower
	 * @param opaque whether the background of the label should be visible or not
	 * @return The new World2D object
	 */
	public World2DObject createNewObject(double x, double y, double width, double height, int layer, boolean opaque) {
		if (layer < 0) layer = 0;
		World2DObject object = new World2DObject(x, y, width, height, layer, opaque);
		return object;
	}
	
	/**
	 * Creates a new World2D Object
	 * @param x The x coordinate at which the object should be in the world 
	 * @param y The y coordinate at which the object should be in the world 
	 * @param width The width of the object. A width of 1 corresponds to the width of a cell 
	 * @param height The height of the object. A height of 1 corresponds to the height of a cell 
	 * @param layer 0 Highest, above lower
	 * @param opaque whether the background of the label should be visible or not
	 * @param texture A texture for the World2D object
	 * @return The new World2D object
	 */
	public World2DObject createNewObject(double x, double y, double width, double height, int layer, boolean opaque, World2DTexture texture) {
		if (layer < 0) layer = 0;
		World2DObject object = new World2DObject(x, y, width, height, layer, texture, opaque);
		return object;
	}
	
	/**
	 * Returns an World2D object
	 * Object can be null
	 * @param key Name of the Object
	 * @return The Object
	 */
	public World2DObject getObject(String key) {
		return objects.get(key);
	}
	
	/**
	 * An Array of all Objects in the world
	 * @return All Objects in the world
	 */
	public World2DObject[] getAllObjects() {
		World2DObject[] objectsArray = new World2DObject[objects.size()];
		String[] keys = getAllObjectsNames();
		for (int i = 0; i < objectsArray.length; i++) {
			objectsArray[i] = objects.get(keys[i]);
		}
		return objectsArray;
	}
	
	/**
	 * An Array of all Object names in the world
	 * @return All Names
	 */
	public String[] getAllObjectsNames() {
		Object[] keys = objects.keySet().toArray();
		return Arrays.copyOf(keys, keys.length, String[].class);
	}
	//END OF METHODS FOR WORLD2D OBJECTS
	
	
	
	
	
	//METHODS FOR WORLD2D TEXTURES
	/**
	 * NOT IMPLEMENTED
	 * @param textures
	 * @param objectWidth
	 * @param objectHeight
	 * @param nextFrameInMillis
	 * @return
	 */
	@Deprecated
	public World2DImgSequenceTexture createNewTextureSequence(ImageIcon[] textures, double objectWidth, double objectHeight, int nextFrameInMillis) {
		//TODO
		return null;
	}
	
	/**
	 * Creates a new Texture sequence but with pre-calculated images
	 * @param originalTexture
	 * @param sizedTextures
	 * @return The texture sequence
	 */
	public World2DImgSequenceTexture createNewTextureSequence(ImageIcon[][] sizedTextures, int nextFrameInMillis) {
		return new World2DImgSequenceTexture(null, sizedTextures, nextFrameInMillis);
	}
	
	/**
	 * Creates a new Texture
	 * @param texture the Image for the texture
	 * @param objectWidth The width of the object which is the texture for
	 * @param objectHeight The height of the object which is the texture for
	 * @return
	 */
	public World2DImgTexture createNewTexture(ImageIcon texture, double objectWidth, double objectHeight) {
		ImageIcon[] textureStages = new ImageIcon[numberScaleStages+1];
		for (float i = 1.0F; i <= maxScale; i += 0.1F) {
			i = Math.round(i*100f)/100f;
			double width = objectWidth*cellPixelSize*i;
			double height = objectHeight*cellPixelSize*i;
			if (width < 1) width = 1;
			if (height < 1) height = 1;
			textureStages[(int) Math.round((i - 1) * 10)] = resizeImage(texture, (int) (width), (int) (height));
		}
		return new World2DImgTexture(texture, textureStages);
	}
	
	/**
	 * Creates a new Texture but with pre-calculated images
	 * @param originalTexture
	 * @param sizedTextures
	 * @return The texture
	 */
	public World2DImgTexture createNewTexture(ImageIcon[] sizedTextures) {
		return new World2DImgTexture(null, sizedTextures);
	}
	
	public void setColorOfObject(Color color, String objectID) {
		objects.get(objectID).setColor(color);
	}
	
	private ImageIcon resizeImage(ImageIcon texture, int width, int height) {
		Image sized = texture.getImage().getScaledInstance(width, height,  Image.SCALE_SMOOTH);
		ImageIcon textureSized = new ImageIcon(sized);
	    return textureSized;
	}
	
	int getDebugGridLineNumber() {
		return verticalLines.length+horizontalLines.length;
	}
	//METHODS FOR WORLD2D TEXTURES
	
	
	
	
	
	//GETTERS ABOUT THE JPANEL AND THE WORLD
	/**
	 * Returns the width of the panel in pixel
	 * @return The width height
	 */
	public int getPanelWidth() {
		return panelWidth;
	}

	void setPanelWidth(int panelWidth) {
		this.panelWidth = panelWidth;
	}

	/**
	 * Returns the X position of the World on the JPanel
	 * @return The X position
	 */
	public int getPanelScreenX() {
		return panelScreenX;
	}

	void setPanelScreenX(int panelScreenX) {
		this.panelScreenX = panelScreenX;
	}

	/**
	 * Returns the Y position of the World on the JPanel
	 * @return The Y position
	 */
	public int getPanelScreenY() {
		return panelScreenY;
	}

	void setPanelScreenY(int panelScreenY) {
		this.panelScreenY = panelScreenY;
	}

	/**
	 * Returns the height of the panel in pixel
	 * @return The panel height
	 */
	public int getPanelHeight() {
		return panelHeight;
	}

	void setPanelHeight(int panelHeight) {
		this.panelHeight = panelHeight;
	}

	/**
	 * Returns the size of a cell in pixel
	 * @return The cell size
	 */
	public int getCellPixelSize() {
		return cellPixelSize;
	}

	void setCellPixelSize(int cellPixelSize) {
		this.cellPixelSize = cellPixelSize;
	}

	/**
	 * Returns the grid width
	 * @return The grid width
	 */
	public double getGridWidth() {
		return gridWidth;
	}

	void setGridWidth(double gridWidth) {
		this.gridWidth = gridWidth;
	}

	/**
	 * Returns the grid height
	 * @return The grid height
	 */
	public double getGridHeight() {
		return gridHeight;
	}

	void setGridHeight(double gridHeight) {
		this.gridHeight = gridHeight;
	}
	
	/**
	 * Returns the value of the max scale
	 * @return The max scale value
	 */
	public int getMaxScale() {
		return maxScale;
	}
	
	public double getWorldOffSetX() {
		return worldOffSetX;
	}

	public void setWorldOffSetX(double worldOffSetX) {
		this.worldOffSetX = worldOffSetX;
	}

	public double getWorldOffSetY() {
		return worldOffSetY;
	}

	public void setWorldOffSetY(double worldOffSetY) {
		this.worldOffSetY = worldOffSetY;
	}
	//END OF GETTERS ABOUT THE JPANEL AND THE WORLD
	
	
	
	

	//DEBUG SETTINGS
	/**
	 * Returns whether the debug screen is visible
	 * @return The visibility of the pause menu
	 */
	public boolean isDebugScreenVisible() {
		return debugScreen.isVisible();
	}
	
	/**
	 * Hides or Shows the debug screen
	 * @param isVisible
	 */
	public void setDebugScreenVisibility(boolean isVisible) {
		debugScreen.setVisible(isVisible);
	}
	
	/**
	 * Returns whether the debug grid is visible
	 * @return The visibility of the debug grid
	 */
	public boolean isDebugGridVisible() {
		return showDebugGrid;
	}
	
	/**
	 * Hides or Shows the debug grid
	 * @param isVisible
	 */
	public void setDebugGridVisibility(boolean isVisible) {
		showDebugGrid = isVisible;
	}
	//END OF DEBUG SETTINGS
	
	/**
	 * Returns whether the pause menu is visible
	 * @return The visibility of the pause menu
	 */
	public boolean isOffScreenVisible() {
		return showOffScreen;
	}
	
	/**
	 * Hides or shows the Pause menu
	 * @param value
	 */
	public void setOfScreenVisibility(boolean value) {
		showOffScreen = value;
	}
	
	/**
	 * Returns the pause menu
	 * @return The off screen
	 */
	public JPanel getOffScreen() {
		return offScreen;
	}
	
	
}
