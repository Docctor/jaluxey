/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package git.bMINUSdocctor.world2D;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;

class World2DDebugScreen {
	
	private JLabel lFps;
	private JLabel lOffSetX;
	private JLabel lOffSetY;
	private JLabel lScaleX;
	private JLabel lScaleY;
	private JLabel lCameraX;
	private JLabel lCameraY;
	private boolean visible;
	
	private int panelHeight;
	
	//POSITION DER LABELS NOCH MACHEN
	
	public World2DDebugScreen(int panelHeight) {
		this.panelHeight = panelHeight;
		lFps = new JLabel();
		lFps.setFont(new Font("Arial", Font.PLAIN, panelHeight/70));
		lFps.setBounds(0, 0, 150, panelHeight/60);
		lFps.setBackground(new Color(0, 0, 0, 120));
		lFps.setForeground(Color.WHITE);
		lFps.setFocusable(false);
		lFps.setOpaque(true);
		lFps.setVisible(false);
		
		lOffSetX = new JLabel();
		lOffSetX.setFont(new Font("Arial", Font.PLAIN, panelHeight/70));
		lOffSetX.setBounds(0, (panelHeight/60)*1, 150, panelHeight/60);
		lOffSetX.setBackground(new Color(0, 0, 0, 120));
		lOffSetX.setForeground(Color.WHITE);
		lOffSetX.setFocusable(false);
		lOffSetX.setOpaque(true);
		lOffSetX.setVisible(false);
		
		lOffSetY = new JLabel();
		lOffSetY.setFont(new Font("Arial", Font.PLAIN, panelHeight/70));
		lOffSetY.setBounds(0, (panelHeight/60)*2, 150, panelHeight/60);
		lOffSetY.setBackground(new Color(0, 0, 0, 120));
		lOffSetY.setForeground(Color.WHITE);
		lOffSetY.setFocusable(false);
		lOffSetY.setOpaque(true);
		lOffSetY.setVisible(false);
		
		lScaleX = new JLabel();
		lScaleX.setFont(new Font("Arial", Font.PLAIN, panelHeight/70));
		lScaleX.setBounds(0, (panelHeight/60)*3, 150, panelHeight/60);
		lScaleX.setBackground(new Color(0, 0, 0, 120));
		lScaleX.setForeground(Color.WHITE);
		lScaleX.setFocusable(false);
		lScaleX.setOpaque(true);
		lScaleX.setVisible(false);
		
		lScaleY = new JLabel();
		lScaleY.setFont(new Font("Arial", Font.PLAIN, panelHeight/70));
		lScaleY.setBounds(0, (panelHeight/60)*4, 150, panelHeight/60);
		lScaleY.setBackground(new Color(0, 0, 0, 120));
		lScaleY.setForeground(Color.WHITE);
		lScaleY.setFocusable(false);
		lScaleY.setOpaque(true);
		lScaleY.setVisible(false);
		
		lCameraX = new JLabel();
		lCameraX.setFont(new Font("Arial", Font.PLAIN, panelHeight/70));
		lCameraX.setBounds(0, (panelHeight/60)*5, 150, panelHeight/60);
		lCameraX.setBackground(new Color(0, 0, 0, 120));
		lCameraX.setForeground(Color.WHITE);
		lCameraX.setFocusable(false);
		lCameraX.setOpaque(true);
		lCameraX.setVisible(false);
		
		lCameraY = new JLabel();
		lCameraY.setFont(new Font("Arial", Font.PLAIN, panelHeight/70));
		lCameraY.setBounds(0, (panelHeight/60)*6, 150, panelHeight/60);
		lCameraY.setBackground(new Color(0, 0, 0, 120));
		lCameraY.setForeground(Color.WHITE);
		lCameraY.setFocusable(false);
		lCameraY.setOpaque(true);
		lCameraY.setVisible(false);
		
		visible = false;
	}
	
	//MORE METHODS

	//GETTERS
	public JLabel getlFps() {
		return lFps;
	}

	public JLabel getlOffSetX() {
		return lOffSetX;
	}

	public JLabel getlOffSetY() {
		return lOffSetY;
	}

	public JLabel getlScaleX() {
		return lScaleX;
	}

	public JLabel getlScaleY() {
		return lScaleY;
	}

	public JLabel getlCameraX() {
		return lCameraX;
	}

	public JLabel getlCameraY() {
		return lCameraY;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
}
