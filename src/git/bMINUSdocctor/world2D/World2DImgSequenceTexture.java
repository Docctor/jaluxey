/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package git.bMINUSdocctor.world2D;

import javax.swing.ImageIcon;

import model.SpriteSheetAnimation;

class World2DImgSequenceTexture implements World2DTexture {

	private ImageIcon[] originalTextureSequence;
	private ImageIcon[][] texturesPreloaded;
	private int nextFrameInMillis;
	private long frameTime = System.currentTimeMillis();
	private SpriteSheetAnimation spriteSheetAnimation;
	
	World2DImgSequenceTexture(ImageIcon[] originalTextures, ImageIcon[][] texturesPreloaded, int nextFrameInMillis) {
		this.originalTextureSequence = originalTextures;
		this.texturesPreloaded = texturesPreloaded;
		this.nextFrameInMillis = nextFrameInMillis;
	}
	
	public ImageIcon getTexture(int frame, int scaleLevel) {
		return texturesPreloaded[frame][scaleLevel];
	}
	
	public ImageIcon[] getOriginalTextureSequence() {
		return originalTextureSequence;
	}
	
	public int getNumberOfFrames() {
		return originalTextureSequence.length;
	}
	
	public int getNextFrameTime() {
		return nextFrameInMillis;
	}
	
	public void setLastFrameTime(long time) {
		this.frameTime = time;
	}
	
	public long getLastFrameTime() {
		return frameTime;
	}
}
