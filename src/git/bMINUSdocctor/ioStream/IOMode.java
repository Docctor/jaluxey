/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package git.bMINUSdocctor.ioStream;

public enum IOMode {
	READ,
	WRITE
}
