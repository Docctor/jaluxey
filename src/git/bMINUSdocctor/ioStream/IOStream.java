/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package git.bMINUSdocctor.ioStream;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;


public class IOStream {
	private File file;
	private BufferedReader reader;
	private BufferedWriter writer;
	private IOMode state;
	
	
	//CONSTRUCTORS
	/**
	 * 
	 * @param file the path of the file file you want to use
	 * @param state Sets the mode. (If you want to read or write the file)
	 * @throws IOException
	 * @throws UnkownIOMode
	 */
	public IOStream(String file, IOMode state) throws IOException, UnkownIOMode {
		
		this.file = new File(file);
		
		if (state == IOMode.READ) {
			Reader reader = new FileReader(this.file);
			this.reader = new BufferedReader(reader);
		} else if (state == IOMode.WRITE) {
			Writer writer = new FileWriter(this.file);
			this.writer = new BufferedWriter(writer);
		} else {
			throw new UnkownIOMode("Cannot parse IOState");
		}
		
		this.state = state;
	}
	
	/**
	 * 
	 * @param file the path of the file file you want to use
	 * @param state Sets the mode. (If you want to read or write the file)
	 * @throws IOException
	 * @throws UnkownIOMode
	 */
	public IOStream(File file, IOMode state) throws IOException, UnkownIOMode {
		
		this.file = file;
		
		if (state == IOMode.READ) {
			Reader reader = new FileReader(this.file);
			this.reader = new BufferedReader(reader);
		} else if (state == IOMode.WRITE) {
			Writer writer = new FileWriter(this.file);
			this.writer = new BufferedWriter(writer);
		} else {
			throw new UnkownIOMode("Cannot parse IOState");
		}
		
		this.state = state;
	}
	
	
	//OTHER METHODS
	
	/**
	 * Adds a line to the writer. If the writer is full, the given file will be written.
	 * This method adds automatically a new line to the String.
	 * @param output The String to write.
	 * @throws IOException
	 * @throws WrongIOMode 
	 */
	public void addLine(String output) throws IOException, WrongIOMode {
		if (state != IOMode.WRITE) throw new WrongIOMode("The method cannot be used because the IOMode is set to READ");
		writer.write(output + "\n");
	}
	
	/**
	 * Writes all Data in the given file.
	 * @throws IOException
	 * @throws WrongIOMode 
	 */
	public void flush() throws IOException, WrongIOMode {
		if (state != IOMode.WRITE) throw new WrongIOMode("The method cannot be used because the IOMode is set to READ");
		writer.flush();
	}
	
	/**
	 * Reads the next line of the file
	 * @return 
	 * @throws IOException
	 * @throws WrongIOMode 
	 */
	public String readNextLine() throws IOException, WrongIOMode {
		if (state != IOMode.READ) throw new WrongIOMode("The method cannot be used because the IOMode is set to WRITE");
		return reader.readLine();
	}
	
	/**
	 * Checks if the file is existing
	 * @param file The specific file
	 * @return true if the file is existing otherwise false
	 */
	public static boolean exists(String file) {
		File fi = new File("file");
		return fi.exists();
	}
	
	/**
	 * Checks if the file is existing
	 * @param file The specific file
	 * @return true if the file is existing otherwise false
	 */
	public static boolean exists(File file) {
		return file.exists();
	}
	
	//GETTERS AND SETTERS
	
	/**
	 * Returns the given file
	 * @return the file
	 */
	public File getFile() {
		return file;
	}
}
