/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package git.bMINUSdocctor.ioStream;

@SuppressWarnings("serial")
public class UnkownIOMode extends Exception {

	public UnkownIOMode(String message) {
		super(message);
	}
}
