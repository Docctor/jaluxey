package git.bMINUSdocctor.ioStream;

@SuppressWarnings("serial")
public class WrongIOMode extends Exception {

	public WrongIOMode(String message) {
		super(message);
	}
}