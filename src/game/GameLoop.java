package game;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import git.bMINUSdocctor.world2D.World2D;
import git.bMINUSdocctor.world2D.World2DObject;
import main.Main;
import system.TextureHandler;
import view.Auswahlrahmen;

public class GameLoop {
	private volatile boolean run;

	private long time = System.currentTimeMillis();
	private int loops = 0;
	
	//DEBUG
	private int fpsNumber = 0;

	private boolean shifIsPessed;
	
	//AUSWAHLRAHMEN
	private Auswahlrahmen auswahlrahmen;
	
	//PANNING AND ZOOMING
	private boolean panningMode = false;
	private float startPanX;
	private float startPanY;
	
	//WORLDPOSITIONS
	private float scaleX = Main.getScaleX(), scaleY = Main.getScaleY(), offSetX = Main.getOffSetX(), offSetY = Main.getOffSetY();
	
	Graphics2D g2d;
	
	public GameLoop() {
	}
	
	public void startGameLoop() {
		run = true;
		auswahlrahmen = new Auswahlrahmen();
		World2DObject object = Main.getGameFrame().getWorld().createNewObject(0, 0, 0, 0, -1, true);
		object.setColor(auswahlrahmen.getColor());
		object.setVisible(false);
		Main.getGameFrame().getWorld().putObjectInWorld(object);
		auswahlrahmen.setWorldObject(object);
		World2D world = Main.getGameFrame().getWorld();
		while (run) {
			System.out.println(TextureHandler.getNumberScrollStage());
			
			if (world == null) return;
			
			try {
				update();
				render();
			} catch (Exception e) {}
			
		}
		
		
	}
	
	public void update() {
		//FPS CALCULATOR
		if (System.currentTimeMillis()-time >= 1000) {
			fpsNumber = loops;
			time = System.currentTimeMillis();
			loops = 0;
		}
		loops++;
		//END OF FPS CALCULATOR
		
		//DEBUG; PANNING; ZOOMING
		Main.getGameFrame().getWorld().setScaleX(scaleX);
		Main.getGameFrame().getWorld().setScaleY(scaleY);
		Main.getGameFrame().getWorld().setOffSetX(offSetX);
		Main.getGameFrame().getWorld().setOffSetY(offSetY);
		Main.getGameFrame().getWorld().setFps(fpsNumber);
		//END OF DEBUG; PANNING; ZOOMING
		
		//AUSWAHLRAHMEN
		if (auswahlrahmen.getWorldObject().isVisible() && auswahlrahmen.isMousePressed()) {
			auswahlrahmen.getWorldObject().setVisible(true);
			World2D world = Main.getGameFrame().getWorld();
			
			int endX = auswahlrahmen.getEndX();
			int startX = auswahlrahmen.getStartX();
			int endY = auswahlrahmen.getEndY();
			int startY = auswahlrahmen.getStartY();
			
			int vectorX = endX-startX;
			int vectorY = endY-startY;
			
			double width = world.widthToWorld(Math.abs(startX-endX));
			double height = world.heightToWorld(Math.abs(startY-endY));
			
			
			if (vectorX > 0 && vectorY > 0) {
				auswahlrahmen.getWorldObject().setX(world.screenToWorldX(startX+Math.abs(startX-endX)/2, width));
				auswahlrahmen.getWorldObject().setY(world.screenToWorldY(startY+Math.abs(startY-endY)/2, height));
			}
			
			if (vectorX < 0 && vectorY < 0) {
				auswahlrahmen.getWorldObject().setX(world.screenToWorldX(endX+Math.abs(startX-endX)/2, width));
				auswahlrahmen.getWorldObject().setY(world.screenToWorldY(endY+Math.abs(startY-endY)/2, height));
			}
			
			if (vectorX > 0 && vectorY < 0) {
				auswahlrahmen.getWorldObject().setX(world.screenToWorldX(startX+Math.abs(startX-endX)/2, width));
				auswahlrahmen.getWorldObject().setY(world.screenToWorldY(endY+Math.abs(startY-endY)/2, height));
			}
			
			if (vectorX < 0 && vectorY > 0) {
				auswahlrahmen.getWorldObject().setX(world.screenToWorldX(endX+Math.abs(startX-endX)/2, width));
				auswahlrahmen.getWorldObject().setY(world.screenToWorldY(startY+Math.abs(startY-endY)/2, height));
			}
			
			auswahlrahmen.getWorldObject().setWidth(width);
			auswahlrahmen.getWorldObject().setHeight(height);
		}
		//END OF AUSWAHLRAHMEN
	}
	
	public void render() {
		
		Main.getGameFrame().getWorld().render();
	}
	
	public ImageIcon resizeImg(JLabel component, ImageIcon img) {
		Image image = img.getImage();
		Image newimg = image.getScaledInstance(component.getWidth(), component.getHeight(),  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		return img = new ImageIcon(newimg);
	}
	
	public void createInputListener() {
		//PANNING LISTENER
		Main.getGameFrame().addMouseListener(new MouseAdapter() {
					@Override
					public void mousePressed(MouseEvent e) {
						if (Main.getGameFrame().getWorld() == null) return;
						if (Main.getGameFrame().getWorld().isOffScreenVisible()) return;
						
						if (e.getButton() == MouseEvent.BUTTON3) {
							panningMode = true;
							startPanX = e.getX();
							startPanY = e.getY();
						}
						
						if (e.getButton() == MouseEvent.BUTTON1) {
							if (auswahlrahmen.getWorldObject().isVisible()) {
								auswahlrahmen.getWorldObject().setVisible(false);
//								uswahlrahmen.get.setAuswahlRahmenPos(0, 0, 0, 0);
							}
								auswahlrahmen.setMousePressed(true);
								auswahlrahmen.setStartX(e.getX());
								auswahlrahmen.setStartY(e.getY());
						}
							
					}
					
					@Override
					public void mouseReleased(MouseEvent e) {
						if (Main.getGameFrame().getWorld() == null) return;
						if (Main.getGameFrame().getWorld().isOffScreenVisible()) return;
						
						if (e.getButton() == MouseEvent.BUTTON3) {
							panningMode = false;
						}
						
						if (e.getButton() == MouseEvent.BUTTON1) {
							auswahlrahmen.setMousePressed(false);
							auswahlrahmen.setEndX(e.getX());
							auswahlrahmen.setEndY(e.getY());
						}
							
					}
					
				});
		Main.getGameFrame().addMouseMotionListener(new MouseMotionAdapter() {
					@Override
					public void mouseDragged(MouseEvent e) {
						if (Main.getGameFrame().getWorld() == null) return;
						if (Main.getGameFrame().getWorld().isOffScreenVisible()) return;
						
						
						if (panningMode) {
							float mouseX = e.getX();
							float mouseY = e.getY();
							
							offSetX = (offSetX-((mouseX-startPanX))/scaleX);
							offSetY = (offSetY-((mouseY-startPanY))/scaleY);
							
							startPanX = mouseX;
							startPanY = mouseY;
						}
						
						if (auswahlrahmen.isMousePressed()) {
							auswahlrahmen.setEndX(e.getX());
							auswahlrahmen.setEndY(e.getY());
							auswahlrahmen.getWorldObject().setVisible(true);
						}
						
					}
//					
//					@Override
//					public void mouseMoved(MouseEvent e) {
//						if (Main.getGameFrame().getWorld() == null) return;
//						if (Main.getGameFrame().getWorld().isOffScreenVisible()) return;
//						System.out.println(auswahlrahmen.isPressed());
//						if (!auswahlrahmen.isPressed()) return;
//						
//						auswahlrahmen.setEndDrawX(e.getX());
//						auswahlrahmen.setEndDrawY(e.getY());
//						auswahlrahmen.setVisible(true);
//					}
				});
				
				//ZOOM LISTENER
		Main.getGameFrame().addMouseWheelListener(new MouseWheelListener() {
					public void mouseWheelMoved(MouseWheelEvent e) {
						if (Main.getGameFrame().getWorld() == null) return;
						
						if (Main.getGameFrame().getWorld().isOffScreenVisible()) return;
						
						int mouseX = e.getX();
						int mouseY = e.getY();
						
						float mouseWorldX_BeforeZoom;
						float mouseWorldY_BeforeZoom;
						
						mouseWorldX_BeforeZoom = screenToWorldX(mouseX);
						mouseWorldY_BeforeZoom = screenToWorldY(mouseY);
						
						if (e.getPreciseWheelRotation() < 0) {
							if (scaleX >= Main.getMaximumScroll() || scaleY > Main.getMaximumScroll()) return;
							//ICH HABE HIER ETWAS VER�NDERT
//							game.setScaleX(game.getScaleX()*1.05f);
//							game.setScaleY(game.getScaleY()*1.05f);
							setScaleX(scaleX + 0.1f);
							setScaleY(scaleY + 0.1f);
						} else {
							if (scaleY <= Main.getMinimumScroll() || scaleY < Main.getMinimumScroll()) return;
//							game.setScaleX(game.getScaleX()*0.95f);
//							game.setScaleY(game.getScaleY()*0.95f);
							setScaleX(scaleX - 0.1f);
							setScaleY(scaleY - 0.1f);
						}
						
						float mouseWorldX_AfterZoom;
						float mouseWorldY_AfterZoom;
						mouseWorldX_AfterZoom = screenToWorldX(mouseX);
						mouseWorldY_AfterZoom = screenToWorldY(mouseY);
						
						offSetX = (offSetX+(mouseWorldX_BeforeZoom-mouseWorldX_AfterZoom));
						offSetY = (offSetY+(mouseWorldY_BeforeZoom-mouseWorldY_AfterZoom));
						
					}
				});
				
				//KEY listener
		Main.getGameFrame().addKeyListener(new KeyAdapter() {
					@Override
					public void keyPressed(KeyEvent e) {
						if (Main.getGameFrame().getWorld() == null) return;
						if (e.getKeyCode() == KeyEvent.VK_ESCAPE && !Main.getGameFrame().getPnlPauseOptionsAudioMenue().isVisible()) {
							boolean offScreen = Main.getGameFrame().getWorld().isOffScreenVisible();
							Main.getGameFrame().getWorld().setOfScreenVisibility(!offScreen);
						}
						
						if (Main.getGameFrame().getWorld().isOffScreenVisible()) return;
						
						if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
							shifIsPessed = true;
						}
						
						if (e.getKeyCode() == KeyEvent.VK_F3) {
							boolean debugScreenVisbility = Main.getGameFrame().getWorld().isDebugScreenVisible();
							Main.getGameFrame().getWorld().setDebugScreenVisibility(!debugScreenVisbility);
							if (!debugScreenVisbility)
								Main.getGameFrame().getWorld().setDebugGridVisibility(false);
							if (shifIsPessed)
								Main.getGameFrame().getWorld().setDebugGridVisibility(true);
							
						}
					}
					@Override
					public void keyReleased(KeyEvent e) {
						if (Main.getGameFrame().getWorld() == null) return;
						if (Main.getGameFrame().getWorld().isOffScreenVisible()) return;
						if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
							shifIsPessed = false;
						}
					}
				});
	}
	
	void setScaleX(float scaleX) {
		this.scaleX = Math.round(scaleX*100)/100.0F;
	}
	
	void setScaleY(float scaleY) {
		this.scaleY = Math.round(scaleY*100)/100.0F;
	}
	
	//METHODS TO TRANSLATE FROM SCREENSPACE TO WORLDSPACE
	public float screenToWorldX(int screenX) {
		return screenX/scaleX + offSetX;
	}

	public float screenToWorldY(int screenY) {
		return screenY/scaleY + offSetY;
	}
	
	public synchronized boolean isRunning() {
		return run;
	}

	public synchronized void setRun(boolean run) {
		this.run = run;
	}
	
	public void resetWorldPosition() {
		scaleX = Main.getScaleX();
		scaleY = Main.getScaleY();
		offSetX = Main.getOffSetX();
		offSetY = Main.getOffSetY();
	}
}
