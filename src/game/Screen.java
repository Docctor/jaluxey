package game;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Screen extends JPanel {

	private static Image image;
	
	public Screen() {
		super();
	}
	
	@Override
	public void paintComponent (Graphics g) {
		super.paintComponent(g);
		g.drawImage(image, 100, 100, null);
	}
	
	public static void setTexture(Image image) {
		Screen.image = image;
	}
	
}
