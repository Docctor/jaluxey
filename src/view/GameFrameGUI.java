package view;

import java.awt.Dimension;
import java.awt.KeyEventDispatcher;
import java.awt.Toolkit;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import game.KeyPoint;
import git.bMINUSdocctor.world2D.TooSmallDimensionException;
import git.bMINUSdocctor.world2D.World2D;
import git.bMINUSdocctor.world2D.World2DObject;
import git.bMINUSdocctor.world2D.World2DTexture;
import main.Main;
import model.Sun;
import system.Arena;
import system.ScreenSelection;
import system.TextureHandler;
import team.TeamColor;

public class GameFrameGUI extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private World2D world;
	
	private float offSetX = 0;
	private float offSetY = 0;
	
	private float scaleX = 1;
	private float scaleY = 1;
	
	//DEBUG
	private JLabel fps, xInfo, yInfo, scaleXInfo, scaleYInfo;
	
	private JPanel contentPane, pnlPauseScreen;
	
	private JPanel pnlMainMenue, pnlOptionMenue, pnlOptionGameMenue, pnlOptionAudioMenue, pnlPauseOptionsAudioMenue;

	//TEXTURES
	World2DTexture sunGreen;
	World2DTexture sunBlue;
	World2DTexture sunOrange;
	World2DTexture sunNone;
	
	
	//panning und zooming i guess HIER WAS VER�NDERT
	private int scrollMax = 3, scrollMin = 1;

	public GameFrameGUI(KeyPoint screen) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
        setUndecorated(true);
        setLocation(screen.getX(), screen.getY());
        setAlwaysOnTop(true);
        
        addFocusListener(new FocusAdapter() {
			private final KeyEventDispatcher altDisabler = new KeyEventDispatcher() {
	            @Override
	            public boolean dispatchKeyEvent(KeyEvent e) {
	                return e.getKeyCode() == 18;
	            }
	        };

	        @Override
	        public void focusGained(FocusEvent e) {
	        	java.awt.KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(altDisabler);
	        }

			@Override
			public void focusLost(FocusEvent e) {
				setState(JFrame.ICONIFIED);
				java.awt.KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(altDisabler);
			}
		});
        contentPane = new JPanel();
	}
	
	public void loadMainMenu() {
		if (world != null) {
			Main.getGameLoop().setRun(false);
			world.removeFrom(contentPane);
			world = null;
		}
		
		if (pnlOptionMenue != null) {
			pnlOptionMenue.setVisible(false);
		}
		
		pnlMainMenue = new GameMainMenue();
		setContentPane(pnlMainMenue);
		validate();
		
	}
	
	public void loadOptionMenue() {
		pnlMainMenue.setVisible(false);
		
		if (pnlOptionGameMenue != null) {
			pnlOptionGameMenue.setVisible(false);
		}
		
		if(pnlOptionAudioMenue != null) {
			pnlOptionAudioMenue.setVisible(false);
		}
	
		pnlOptionMenue = new GameOptionsMenue();
		
		setContentPane(pnlOptionMenue);
	}
	
	public void loadOptionGameMenue() {
		pnlOptionMenue.setVisible(false);
		pnlOptionGameMenue = new GameOptionsGameMenue();
		
		setContentPane(pnlOptionGameMenue);
	}
	
	public void loadOptionAudioMenue() {
		pnlOptionMenue.setVisible(false);
		pnlOptionAudioMenue = new GameOptionsAudioMenue();
		
		setContentPane(pnlOptionAudioMenue);
	}
	
	public void loadGame(String id) {
		pnlMainMenue.setVisible(false);
		setContentPane(contentPane);
		//HIER MAIN SCREEN
		
		Main.getGameLoop().resetWorldPosition();
		
		JPanel optionsMenu = new JPanel();
		optionsMenu.setOpaque(false);
		optionsMenu.setLayout(null);
		
		pnlPauseOptionsAudioMenue = new GamePauseOptionsAudioMenue();
		pnlPauseOptionsAudioMenue.setVisible(false);
		optionsMenu.add(pnlPauseOptionsAudioMenue);
		
		pnlPauseScreen = new GamePauseScreen();
		optionsMenu.add(pnlPauseScreen);
		
		int width = ScreenSelection.getInstance().getScreenWidth();
		int height = ScreenSelection.getInstance().getScreenHeight();
		
		try {
			world = new World2D(0, 0, width, height,16, 16, 50, 5, 0.1, optionsMenu);
			world.displayOn(contentPane);
			
			sunNone = world.createNewTexture(TextureHandler.getImageIconArray("sun_sized"));
			sunBlue = world.createNewTexture(TextureHandler.getImageIconArray("sun_sized_blue"));
			sunGreen = world.createNewTexture(TextureHandler.getImageIconArray("sun_sized_green"));
			sunOrange = world.createNewTexture(TextureHandler.getImageIconArray("sun_sized_orange"));
			
			Arena arena = Arena.getArena(id);
			ArrayList<Sun> suns = arena.getSuns();
			for (int i = 0; i < suns.size(); i++) {
				Sun sun = suns.get(i);
				float x = sun.getX();
				float y = sun.getY();
				TeamColor team = sun.getTeam();
				World2DObject object = world.createNewObject(x, y, 1, 1, 0, false, getTextureByTeam(team));
				world.putObjectInWorld(object);
				sun.setWorld2DObject(object);
				Sun.allSuns.put(object.getUniqueID().toString(), sun);
			}
			
			Thread t1 = new Thread() {
				@Override
				public void run() {
					Main.getGameLoop().startGameLoop();
				}
			};
			t1.start();
		} catch (TooSmallDimensionException e) {
			e.printStackTrace();
		}
	}
	
	private World2DTexture getTextureByTeam(TeamColor team) {
		switch (team) {
		case NONE:
			return sunNone;
		case GREEN:
			return sunGreen;
		case BLUE:
			return sunBlue;
		case ORANGE:
			return sunOrange;
		default:
			throw new IllegalArgumentException("Unexpected value: " + team);
		}
	}
	
	public World2D getWorld() {
		return world;
	}
	
	public JPanel getContentPane() {
		return contentPane;
	}
	
	public int worldToScreenX(float worldX) {
		return (int) ((worldX - offSetX) * scaleX);
	}

	public int worldToScreenY(float worldY) {
		return (int) ((worldY - offSetY) * scaleY);
	}
	
	public float screenToWorldX(int screenX) {
		return screenX/scaleX + offSetX;
	}

	public float screenToWorldY(int screenY) {
		return screenY/scaleY + offSetY;
	}

	public float getOffSetX() {
		return offSetX;
	}

	public void setOffSetX(float offSetX) {
		this.offSetX = offSetX;
	}

	public float getOffSetY() {
		return offSetY;
	}

	public void setOffSetY(float offSetY) {
		this.offSetY = offSetY;
	}

	public float getScaleY() {
		return scaleY;
	}

	public void setScaleY(float scaleY) {
		this.scaleY = Math.round(scaleY*100)/100.0F;
	}

	public float getScaleX() {
		return scaleX;
	}

	public void setScaleX(float scaleX) {
		this.scaleX = Math.round(scaleX*100)/100.0F;
	}

	
	//DEBUG
	public JLabel getFpsLabel() {
		return fps;
	}

	public void setFpsLabel(JLabel fps) {
		this.fps = fps;
	}

	public JLabel getXInfoLabel() {
		return xInfo;
	}

	public void setXInfoLabel(JLabel xInfo) {
		this.xInfo = xInfo;
	}

	public JLabel getYInfoLabel() {
		return yInfo;
	}

	public void setYInfoLabel(JLabel yInfo) {
		this.yInfo = yInfo;
	}

	public JLabel getScaleXInfoLabel() {
		return scaleXInfo;
	}

	public void setScaleXInfoLabel(JLabel scaleXInfo) {
		this.scaleXInfo = scaleXInfo;
	}

	public JLabel getScaleYInfoLabel() {
		return scaleYInfo;
	}

	public void setScaleYInfoLabel(JLabel scaleYInfo) {
		this.scaleYInfo = scaleYInfo;
	}
	//END OF DEBUG
	
	//panning and zooming i guess HIER WAS VER�NDERT
	public int getScrollMax() {
		return scrollMax;
	}

	public void setScrollMax(int scrollMax) {
		this.scrollMax = scrollMax;
	}

	public int getScrollMin() {
		return scrollMin;
	}

	public void setScrollMin(int scrollMin) {
		this.scrollMin = scrollMin;
	}

	public JPanel getPnlPauseScreen() {
		return pnlPauseScreen;
	}
	
	public JPanel getPnlPauseOptionsAudioMenue() {
		return pnlPauseOptionsAudioMenue;
	}
	
}
