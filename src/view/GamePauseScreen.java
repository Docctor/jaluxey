package view;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import main.Main;
import system.ScreenSelection;
import system.SoundHandler;
import system.TextureHandler;

public class GamePauseScreen extends JPanel {

	Font labelFont = TextureHandler.getFont(0).deriveFont(30f);

	private Color backgroundColor = new Color(0, 0, 0, 225);
	
	private Image backgroundImage = ScreenSelection.getInstance().resizeToScreenSize("PauseScreenBackground").getImage();
	
	private int width = ScreenSelection.getInstance().getScreenWidth(), height = ScreenSelection.getInstance().getScreenHeight(), yStartingValue = ScreenSelection.getInstance().getYStartingValue();
	
	private Robot robot;
	
	public GamePauseScreen() {

		//Erstellung des PauseScreens
		
		
		setBounds(0, 0, width, height);
		setBackground(backgroundColor);
		
		//Erstellung des Rasters
		GridBagLayout gbl_pnlPauseScreen = new GridBagLayout();
		
		gbl_pnlPauseScreen.columnWidths = new int[]{0, 0, 0};
		gbl_pnlPauseScreen.rowHeights = new int[]{0, 0, 0};
		gbl_pnlPauseScreen.columnWeights = new double[]{0.0, 0.0, 0.0};
		gbl_pnlPauseScreen.rowWeights = new double[]{0.0, 0.0, 0.0};
		setLayout(gbl_pnlPauseScreen);
		
		//Oberes Iconlabel
		
		
		JLabel lblTopGreater = new JLabel(">");
		
		lblTopGreater.setVisible(false);
		lblTopGreater.setFont(labelFont);
		lblTopGreater.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblTopGreater = new GridBagConstraints();
		gbc_lblTopGreater.insets = new Insets(0, 0, 25, 0);
		gbc_lblTopGreater.gridx = 0;
		gbc_lblTopGreater.gridy = 0;
		add(lblTopGreater, gbc_lblTopGreater);
		
		JLabel lblMidGreater = new JLabel(">");
		
		lblMidGreater.setVisible(false);
		lblMidGreater.setFont(labelFont);
		lblMidGreater.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblMidGreater = new GridBagConstraints();
		gbc_lblMidGreater.insets = new Insets(0, 0, 25, 0);
		gbc_lblMidGreater.gridx = 0;
		gbc_lblMidGreater.gridy = 1;
		add(lblMidGreater, gbc_lblMidGreater);
		
		JLabel lblBotGreater = new JLabel(">");
		
		lblBotGreater.setVisible(false);
		lblBotGreater.setFont(labelFont);
		lblBotGreater.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblBotGreater = new GridBagConstraints();
		gbc_lblBotGreater.gridx = 0;
		gbc_lblBotGreater.gridy = 2;
		add(lblBotGreater, gbc_lblBotGreater);
		
		JLabel lblTopLesser = new JLabel("<");
		
		lblTopLesser.setVisible(false);
		lblTopLesser.setFont(labelFont);
		lblTopLesser.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblTopLesser = new GridBagConstraints();
		gbc_lblTopLesser.insets = new Insets(0, 0, 25, 0);
		gbc_lblTopLesser.gridx = 2;
		gbc_lblTopLesser.gridy = 0;
		add(lblTopLesser, gbc_lblTopLesser);
		
		JLabel lblMidLesser = new JLabel("<");
		
		lblMidLesser.setVisible(false);
		lblMidLesser.setFont(labelFont);
		lblMidLesser.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblMidLesser = new GridBagConstraints();
		gbc_lblMidLesser.insets = new Insets(0, 0, 25, 0);
		gbc_lblMidLesser.gridx = 2;
		gbc_lblMidLesser.gridy = 1;
		add(lblMidLesser, gbc_lblMidLesser);
		
		JLabel lblBotLesser = new JLabel("<");
		
		lblBotLesser.setVisible(false);
		lblBotLesser.setFont(labelFont);
		lblBotLesser.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblBotLesser = new GridBagConstraints();
		gbc_lblBotLesser.gridx = 2;
		gbc_lblBotLesser.gridy = 2;
		add(lblBotLesser, gbc_lblBotLesser);
		
		//Continue Textlabel
		JLabel lblContinue = new JLabel("Continue");
	
		lblContinue.setForeground(Color.WHITE);
		lblContinue.setFont(labelFont);
		
		GridBagConstraints gbc_lblContinue = new GridBagConstraints();
		gbc_lblContinue.insets = new Insets(0, 0, 25, 0);
		gbc_lblContinue.gridx = 1;
		gbc_lblContinue.gridy = 0;
		add(lblContinue, gbc_lblContinue);
		
		//Option Textlabel
		JLabel lblOptions = new JLabel("Options");
		
		lblOptions.setForeground(Color.WHITE);
		lblOptions.setFont(labelFont);
		
		GridBagConstraints gbc_lblOptions = new GridBagConstraints();
		gbc_lblOptions.insets = new Insets(0, 0, 25, 0);
		gbc_lblOptions.gridx = 1;
		gbc_lblOptions.gridy = 1;
		add(lblOptions, gbc_lblOptions);
		
		//Quit To Menue Textlabel
		JLabel lblQuitToMenue = new JLabel("Quit To Menue");
			
		lblQuitToMenue.setForeground(Color.WHITE);
		lblQuitToMenue.setFont(labelFont);
		
		GridBagConstraints gbc_lblQuitToMenue = new GridBagConstraints();
		gbc_lblQuitToMenue.gridx = 1;
		gbc_lblQuitToMenue.gridy = 2;
		add(lblQuitToMenue, gbc_lblQuitToMenue);
		
		//Listener //Es fehlen noch die Funktionen f�r die Listener lblOptions und lblQuitToMenue
		lblContinue.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				SoundHandler.playSound("uiButtonClick", Integer.parseInt(Main.getOptions().getOption("sound.volume")));
				if (e.getButton() != MouseEvent.BUTTON1) return;
				try {
					robot = new Robot();
					robot.keyPress(KeyEvent.VK_ESCAPE);
					robot.keyRelease(KeyEvent.VK_ESCAPE);
				} catch (AWTException e2) {

				}
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				lblTopGreater.setVisible(true);
				lblTopLesser.setVisible(true);
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				lblTopGreater.setVisible(false);
				lblTopLesser.setVisible(false);
			}
		});
		
		lblOptions.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				SoundHandler.playSound("uiButtonClick", Integer.parseInt(Main.getOptions().getOption("sound.volume")));
				Main.getGameFrame().getPnlPauseOptionsAudioMenue().setVisible(true);
				setVisible(false);
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				lblMidGreater.setVisible(true);
				lblMidLesser.setVisible(true);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblMidGreater.setVisible(false);
				lblMidLesser.setVisible(false);
			}
		});
		
		lblQuitToMenue.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				SoundHandler.playSound("uiButtonClick", Integer.parseInt(Main.getOptions().getOption("sound.volume")));
				if (e.getButton() != MouseEvent.BUTTON1) return;
				Main.getGameFrame().loadMainMenu();
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				lblBotGreater.setVisible(true);
				lblBotLesser.setVisible(true);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblBotGreater.setVisible(false);
				lblBotLesser.setVisible(false);
			}
		});
		
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(backgroundImage, 0, yStartingValue, null);
	}
}
