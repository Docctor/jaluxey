package view;

import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import main.Main;
import system.ScreenSelection;
import system.SoundHandler;
import system.TextureHandler;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GameOptionsAudioMenue extends JPanel {

	private Font labelFont = TextureHandler.getFont(0).deriveFont(30f);

	private Image backgroundImage = ScreenSelection.getInstance().resizeToScreenSize("GameOptionsAudioMenue").getImage();
	private int width = ScreenSelection.getInstance().getScreenWidth(), height = ScreenSelection.getInstance().getScreenHeight(), yStartingValue = ScreenSelection.getInstance().getYStartingValue();
	
	private Dimension labelDimension = new Dimension(240, 50);
	/**
	 * Create the panel.
	 */
	public GameOptionsAudioMenue() {
		
		setBounds(0, 0, width, height);
		setBackground(Color.BLACK);
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0};
		setLayout(gridBagLayout);
		
		JLabel lblVolume = new JLabel("Volume");
		
		lblVolume.setFont(labelFont);
		lblVolume.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblVolume = new GridBagConstraints();
		gbc_lblVolume.insets = new Insets(0, 0, 0, 25);
		gbc_lblVolume.gridx = 0;
		gbc_lblVolume.gridy = 0;
		add(lblVolume, gbc_lblVolume);

		JLabel lblVolumeSelectionLeftArrow = new JLabel("<--");
		
		lblVolumeSelectionLeftArrow.setFont(labelFont);
		lblVolumeSelectionLeftArrow.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblVolumeSelectionLeftArrow = new GridBagConstraints();
		gbc_lblVolumeSelectionLeftArrow.gridx = 1;
		gbc_lblVolumeSelectionLeftArrow.gridy = 0;
		add(lblVolumeSelectionLeftArrow, gbc_lblVolumeSelectionLeftArrow);
		
		JLabel lblVolumeSelected = new JLabel(Main.getOptions().getOption("sound.volume"));
		
		lblVolumeSelected.setHorizontalAlignment(JLabel.CENTER);
		lblVolumeSelected.setMinimumSize(labelDimension);
		lblVolumeSelected.setPreferredSize(labelDimension);
		lblVolumeSelected.setMaximumSize(labelDimension);
		lblVolumeSelected.setFont(labelFont);
		lblVolumeSelected.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblVolumeSelected = new GridBagConstraints();
		gbc_lblVolumeSelected.insets = new Insets(0, 25, 0, 25);
		gbc_lblVolumeSelected.gridx = 2;
		gbc_lblVolumeSelected.gridy = 0;
		add(lblVolumeSelected, gbc_lblVolumeSelected);
		
		JLabel lblVolumeSelectionRightArrow = new JLabel("-->");
		
		lblVolumeSelectionRightArrow.setFont(labelFont);
		lblVolumeSelectionRightArrow.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblVolumeSelectionRightArrow = new GridBagConstraints();
		gbc_lblVolumeSelectionRightArrow.gridx = 3;
		gbc_lblVolumeSelectionRightArrow.gridy = 0;
		add(lblVolumeSelectionRightArrow, gbc_lblVolumeSelectionRightArrow);
		
		Main.getGameFrame().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if ( isVisible() && e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					Main.getGameFrame().loadOptionMenue();
				}
			}
		});
		
		lblVolumeSelectionLeftArrow.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				int volumeSelectedTemp = Integer.parseInt(lblVolumeSelected.getText());
				SoundHandler.playSound("uiButtonClick", Integer.parseInt(Main.getOptions().getOption("sound.volume")));
				if ((e.getModifiersEx() & MouseEvent.SHIFT_DOWN_MASK) != 0) {
					if ((volumeSelectedTemp - 10) < 0) {
						volumeSelectedTemp = 0;
						lblVolumeSelected.setText(volumeSelectedTemp + "");
						Main.getOptions().setOption("sound.volume", lblVolumeSelected.getText());
						return;
					} else {
						volumeSelectedTemp -= 10;
						lblVolumeSelected.setText(volumeSelectedTemp + "");
						Main.getOptions().setOption("sound.volume", lblVolumeSelected.getText());
						return;
					}
				} else if (volumeSelectedTemp == 0) {
					return;
				} else {
					volumeSelectedTemp--;
					lblVolumeSelected.setText(volumeSelectedTemp + "");
					Main.getOptions().setOption("sound.volume", lblVolumeSelected.getText());
					return;
				}
			}
		});
		
		lblVolumeSelectionRightArrow.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				
				int volumeSelectedTemp = Integer.parseInt(lblVolumeSelected.getText());
				SoundHandler.playSound("uiButtonClick", Integer.parseInt(Main.getOptions().getOption("sound.volume")));
				if ((e.getModifiersEx() & MouseEvent.SHIFT_DOWN_MASK) != 0) {
					if ((volumeSelectedTemp + 10) > 100) {
						volumeSelectedTemp = 100;
						lblVolumeSelected.setText(volumeSelectedTemp + "");
						Main.getOptions().setOption("sound.volume", lblVolumeSelected.getText());
						return;
					} else {
						volumeSelectedTemp += 10;
						lblVolumeSelected.setText(volumeSelectedTemp + "");
						Main.getOptions().setOption("sound.volume", lblVolumeSelected.getText());
						return;
					}
					
				} else if(volumeSelectedTemp == 100) {
						return;
				} else {
						volumeSelectedTemp++;
						lblVolumeSelected.setText(volumeSelectedTemp + "");
						Main.getOptions().setOption("sound.volume", lblVolumeSelected.getText());
						return;
				}
			}
		});
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(backgroundImage, 0, yStartingValue, null);
	}
}
