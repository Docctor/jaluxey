package view;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import system.Arena;
import system.ScreenSelection;
import system.SoundHandler;
import system.TextureHandler;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import main.Main;

public class GameOptionsGameMenue extends JPanel {

	private Font titleFont = TextureHandler.getFont(0).deriveFont(80f);
	private Font labelFont = TextureHandler.getFont(0).deriveFont(30f);

	private String colorList[] = {"blue", "green", "orange"};
	private String arenaList[] = {"standard", "arena 2", "arena 3"};
	
	private Dimension labelDimension = new Dimension(240, 50);
	
	private Image backgroundImage = ScreenSelection.getInstance().resizeToScreenSize("GameOptionsGameMenue").getImage();
	
	private int currentColor = 0, currentArena = 0, width = ScreenSelection.getInstance().getScreenWidth(), height = ScreenSelection.getInstance().getScreenHeight(), yStartingValue = ScreenSelection.getInstance().getYStartingValue();
	
	/**
	 * Create the panel.
	 */
	public GameOptionsGameMenue() {
		
		setBounds(0, 0, width, height);
		setBackground(Color.BLACK);
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0};
		setLayout(gridBagLayout);
		
		JLabel lblTeamSelection = new JLabel("Team");
		
		lblTeamSelection.setFont(labelFont);
		lblTeamSelection.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblTeamSelection = new GridBagConstraints();
		gbc_lblTeamSelection.insets = new Insets(0, 0, 25, 25);
		gbc_lblTeamSelection.gridx = 0;
		gbc_lblTeamSelection.gridy = 1;
		add(lblTeamSelection, gbc_lblTeamSelection);
		
		JLabel lblTeamSelectionLeftArrow = new JLabel("<--");
		
		lblTeamSelectionLeftArrow.setFont(labelFont);
		lblTeamSelectionLeftArrow.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblTeamSelectionLeftArrow = new GridBagConstraints();
		gbc_lblTeamSelectionLeftArrow.insets = new Insets(0, 0, 25, 0);
		gbc_lblTeamSelectionLeftArrow.gridx = 1;
		gbc_lblTeamSelectionLeftArrow.gridy = 1;
		add(lblTeamSelectionLeftArrow, gbc_lblTeamSelectionLeftArrow);
		
		JLabel lblTeamSelected = new JLabel(colorList[currentColor], SwingConstants.CENTER);
		
		lblTeamSelected.setMinimumSize(labelDimension);
		lblTeamSelected.setPreferredSize(labelDimension);
		lblTeamSelected.setMaximumSize(labelDimension);
		lblTeamSelected.setFont(labelFont);
		lblTeamSelected.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblTeamSelected = new GridBagConstraints();
		gbc_lblTeamSelected.insets = new Insets(0, 25, 25, 25);
		gbc_lblTeamSelected.gridx = 2;
		gbc_lblTeamSelected.gridy = 1;
		add(lblTeamSelected, gbc_lblTeamSelected);
		
		JLabel lblTeamSelectionRightArrow = new JLabel("-->");
		
		lblTeamSelectionRightArrow.setFont(labelFont);
		lblTeamSelectionRightArrow.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblTeamSelectionRightArrow = new GridBagConstraints();
		gbc_lblTeamSelectionRightArrow.insets = new Insets(0, 0, 25, 0);
		gbc_lblTeamSelectionRightArrow.gridx = 3;
		gbc_lblTeamSelectionRightArrow.gridy = 1;
		add(lblTeamSelectionRightArrow, gbc_lblTeamSelectionRightArrow);
		
		JLabel lblArenaSelection = new JLabel("Arena");
		
		lblArenaSelection.setFont(labelFont);
		lblArenaSelection.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblArenaSelection = new GridBagConstraints();
		gbc_lblArenaSelection.insets = new Insets(0, 0, 0, 25);
		gbc_lblArenaSelection.gridx = 0;
		gbc_lblArenaSelection.gridy = 2;
		add(lblArenaSelection, gbc_lblArenaSelection);
		
		JLabel lblArenaSelectionLeftArrow = new JLabel("<--");
		
		lblArenaSelectionLeftArrow.setFont(labelFont);
		lblArenaSelectionLeftArrow.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblArenaSelectionLeftArrow = new GridBagConstraints();
		gbc_lblArenaSelectionLeftArrow.gridx = 1;
		gbc_lblArenaSelectionLeftArrow.gridy = 2;
		add(lblArenaSelectionLeftArrow, gbc_lblArenaSelectionLeftArrow);
		
		JLabel lblArenaSelected = new JLabel(arenaList[currentArena]);
		
		lblArenaSelected.setHorizontalAlignment(JLabel.CENTER);
		lblArenaSelected.setMinimumSize(labelDimension);
		lblArenaSelected.setPreferredSize(labelDimension);
		lblArenaSelected.setMaximumSize(labelDimension);
		lblArenaSelected.setFont(labelFont);
		lblArenaSelected.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblArenaSelected = new GridBagConstraints();
		gbc_lblArenaSelected.insets = new Insets(0, 25, 0, 25);
		gbc_lblArenaSelected.gridx = 2;
		gbc_lblArenaSelected.gridy = 2;
		add(lblArenaSelected, gbc_lblArenaSelected);
		
		JLabel lblArenaSelectionRightArrow = new JLabel("-->");
		
		lblArenaSelectionRightArrow.setFont(labelFont);
		lblArenaSelectionRightArrow.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblArenaSelectionRightArrow = new GridBagConstraints();
		gbc_lblArenaSelectionRightArrow.gridx = 3;
		gbc_lblArenaSelectionRightArrow.gridy = 2;
		add(lblArenaSelectionRightArrow, gbc_lblArenaSelectionRightArrow);
		
		Main.getGameFrame().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if ( isVisible() && e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					Main.getGameFrame().loadOptionMenue();
				}
			}
		});
		
		lblTeamSelectionLeftArrow.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				SoundHandler.playSound("uiButtonClick", Integer.parseInt(Main.getOptions().getOption("sound.volume")));
				if (currentColor - 1 < 0) {
					currentColor = colorList.length-1;
				} else {
					currentColor = currentColor - 1;	
				}
				 
				lblTeamSelected.setText(colorList[currentColor]);
				Main.getOptions().setOption("game.teamColor", lblTeamSelected.getText());
			}
		});
		
		lblTeamSelectionRightArrow.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				SoundHandler.playSound("uiButtonClick", Integer.parseInt(Main.getOptions().getOption("sound.volume")));
				if (currentColor + 1 > colorList.length-1) {
					currentColor = 0;
				} else {
					currentColor += 1;
				}
				 
				lblTeamSelected.setText(colorList[currentColor]);
				Main.getOptions().setOption("game.teamColor", lblTeamSelected.getText());
			}
		});
		
		lblArenaSelectionLeftArrow.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				SoundHandler.playSound("uiButtonClick", Integer.parseInt(Main.getOptions().getOption("sound.volume")));
				if (currentArena - 1 < 0) {
					currentArena = arenaList.length-1;
				} else {
					currentArena = currentArena - 1;	
				}
				 
				lblArenaSelected.setText(arenaList[currentArena]);
				Main.getOptions().setOption("game.selectedArena", lblArenaSelected.getText());
			}
		});
		
		lblArenaSelectionRightArrow.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				SoundHandler.playSound("uiButtonClick", Integer.parseInt(Main.getOptions().getOption("sound.volume")));
				if (currentArena + 1 > arenaList.length-1) {
					currentArena = 0;
				} else {
					currentArena += 1;
				}
				 
				lblArenaSelected.setText(arenaList[currentArena]);
				Main.getOptions().setOption("game.selectedArena", lblArenaSelected.getText());
			}
		});

//		ActionListener lblTeamSelectedActionListener = new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				String teamColor = (String) lblTeamSelected.getText();
//				
//				switch (teamColor) {
//				case "blue": 
//					Main.getOptions().setOption("game.teamColor", teamColor.toUpperCase());
//					break;
//				case "green":
//					Main.getOptions().setOption("game.teamColor", teamColor.toUpperCase());
//					break;
//				case "orange":
//					Main.getOptions().setOption("game.teamColor", teamColor.toUpperCase());
//					break;
//				default:
//					System.out.println("Unexpected value: " + teamColor);
//				}
//			}
//		};
//		lblArenaSelected.addActionListener(lblTeamSelectedActionListener);
//		
//		ActionListener cbArenaSelectionActionListener = new ActionListener() {
//			@Override public void actionPerformed(ActionEvent e) {
//				String arena = (String) cbArenaSelection.getSelectedItem();
//				
//				switch (arena) {
//				case "standart":
//					Main.getOptions().setOption("game.selectedArena", arena);
//					break;
//				case "arena 2":
//					Main.getOptions().setOption("game.selectedArena", arena);
//					break;
//				case "arena 3":
//					Main.getOptions().setOption("game.selectedArena", arena);
//					break;
//				default:
//					System.out.println("Unexpected value: " + arena);
//					break;
//				}
//			}
//		};
//		cbArenaSelection.addActionListener(cbArenaSelectionActionListener);
		
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(backgroundImage, 0, yStartingValue, null);
	}
}
