package view;

import java.awt.Color;

import git.bMINUSdocctor.world2D.World2DObject;

public class Auswahlrahmen {
	private Color color = new Color(0, 0, 255, 40);
	private int startX, startY, endX, endY;
	private double worldX = 0, worldY = 0, worldHeight = 0, worldWidth = 0;
	private World2DObject object;
	private boolean mousePressed = false;
	
	/**
	 * Create the panel.
	 */
	public Auswahlrahmen() {
	}


	public int getStartX() {
		return startX;
	}


	public void setStartX(int startX) {
		this.startX = startX;
	}


	public int getStartY() {
		return startY;
	}


	public void setStartY(int startY) {
		this.startY = startY;
	}


	public int getEndX() {
		return endX;
	}


	public void setEndX(int endX) {
		this.endX = endX;
	}


	public int getEndY() {
		return endY;
	}


	public void setEndY(int endY) {
		this.endY = endY;
	}


	public double getWorldX() {
		return worldX;
	}


	public void setWorldX(double x) {
		this.worldX = x;
	}


	public double getWorldY() {
		return worldY;
	}


	public void setWorldY(double y) {
		this.worldY = y;
	}


	public double getWorldHeight() {
		return worldHeight;
	}


	public void setWorldHeight(double worldHeight) {
		this.worldHeight = worldHeight;
	}


	public double getWorldWidth() {
		return worldWidth;
	}


	public void setWorldWidth(double worldWidth) {
		this.worldWidth = worldWidth;
	}


	public World2DObject getWorldObject() {
		return object;
	}


	public void setWorldObject(World2DObject object) {
		this.object = object;
	}

	public Color getColor() {
		return color;
	}


	public void setColor(Color color) {
		this.color = color;
	}


	public boolean isMousePressed() {
		return mousePressed;
	}


	public void setMousePressed(boolean mousePressed) {
		this.mousePressed = mousePressed;
	}
	
}
