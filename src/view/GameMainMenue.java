package view;

import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.JPanel;

import main.Main;
import system.ScreenSelection;
import system.SoundHandler;
import system.TextureHandler;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class GameMainMenue extends JPanel {

	private Font labelFont = TextureHandler.getFont(0).deriveFont(30f);
	
	private Image backgroundImage = ScreenSelection.getInstance().resizeToScreenSize("MainMenueTitelIcon").getImage();
	
	private int width = ScreenSelection.getInstance().getScreenWidth(), height = ScreenSelection.getInstance().getScreenHeight(), yStartingValue = ScreenSelection.getInstance().getYStartingValue();
	
	private boolean startGame = false;
	
	/**
	 * Create the panel.
	 */
	public GameMainMenue() {

		//Hintergrundbild noch machen

		setBackground(Color.BLACK);
		
		GridBagLayout gbl_GameMainMenue = new GridBagLayout();
		
		gbl_GameMainMenue.columnWidths = new int[]{0, 0, 0};
		gbl_GameMainMenue.rowHeights = new int[]{0, 0, 0};
		gbl_GameMainMenue.columnWeights = new double[]{0.0, 0.0, 0.0};
		gbl_GameMainMenue.rowWeights = new double[]{0.0, 0.0, 0.0};
		setLayout(gbl_GameMainMenue);
		
		
		JLabel lblTopGreater = new JLabel(">");
		
		lblTopGreater.setVisible(false);
		lblTopGreater.setFont(labelFont);
		lblTopGreater.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblTopGreater = new GridBagConstraints();
		gbc_lblTopGreater.insets = new Insets(height / 2, 0, 25, 0);
		gbc_lblTopGreater.gridx = 0;
		gbc_lblTopGreater.gridy = 0;
		add(lblTopGreater, gbc_lblTopGreater);
		
		JLabel lblMidGreater = new JLabel(">");
		
		lblMidGreater.setVisible(false);
		lblMidGreater.setFont(labelFont);
		lblMidGreater.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblMidGreater = new GridBagConstraints();
		gbc_lblMidGreater.insets = new Insets(0, 0, 25, 0);
		gbc_lblMidGreater.gridx = 0;
		gbc_lblMidGreater.gridy = 1;
		add(lblMidGreater, gbc_lblMidGreater);
		
		JLabel lblBotGreater = new JLabel(">");
		
		lblBotGreater.setVisible(false);
		lblBotGreater.setFont(labelFont);
		lblBotGreater.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblBotGreater = new GridBagConstraints();
		gbc_lblBotGreater.insets = new Insets(0, 0, 0, 0);
		gbc_lblBotGreater.gridx = 0;
		gbc_lblBotGreater.gridy = 2;
		add(lblBotGreater, gbc_lblBotGreater);
		
		JLabel lblTopLesser = new JLabel("<");
		
		lblTopLesser.setVisible(false);
		lblTopLesser.setFont(labelFont);
		lblTopLesser.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblTopLesser = new GridBagConstraints();
		gbc_lblTopLesser.insets = new Insets(height / 2, 0, 25, 0);
		gbc_lblTopLesser.gridx = 2;
		gbc_lblTopLesser.gridy = 0;
		add(lblTopLesser, gbc_lblTopLesser);
		
		JLabel lblMidLesser = new JLabel("<");
		
		lblMidLesser.setVisible(false);
		lblMidLesser.setFont(labelFont);
		lblMidLesser.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblMidLesser = new GridBagConstraints();
		gbc_lblMidLesser.insets = new Insets(0, 0, 25, 0);
		gbc_lblMidLesser.gridx = 2;
		gbc_lblMidLesser.gridy = 1;
		add(lblMidLesser, gbc_lblMidLesser);
		
		JLabel lblBotLesser = new JLabel("<");
		
		lblBotLesser.setVisible(false);
		lblBotLesser.setFont(labelFont);
		lblBotLesser.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblBotLesser = new GridBagConstraints();
		gbc_lblBotLesser.insets = new Insets(0, 0, 0, 0);
		gbc_lblBotLesser.gridx = 2;
		gbc_lblBotLesser.gridy = 2;
		add(lblBotLesser, gbc_lblBotLesser);
		
		JLabel lblStartGame = new JLabel("Start Game");
		
		lblStartGame.setFont(labelFont);
		lblStartGame.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblStartGame = new GridBagConstraints();
		gbc_lblStartGame.insets = new Insets(height / 2, 10, 25, 10);
		gbc_lblStartGame.gridx = 1;
		gbc_lblStartGame.gridy = 0;
		add(lblStartGame, gbc_lblStartGame);
		
		JLabel lblOptions = new JLabel("Options");
		
		lblOptions.setFont(labelFont);
		lblOptions.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblOptions = new GridBagConstraints();
		gbc_lblOptions.insets = new Insets(0, 10, 25, 10);
		gbc_lblOptions.gridx = 1;
		gbc_lblOptions.gridy = 1;
		add(lblOptions, gbc_lblOptions);
		
		JLabel lblQuitGame = new JLabel("Quit Game");
		
		lblQuitGame.setFont(labelFont);
		lblQuitGame.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_QuitGame = new GridBagConstraints();
		gbc_QuitGame.insets = new Insets(0, 10, 0, 10);
		gbc_QuitGame.gridx = 1;
		gbc_QuitGame.gridy = 2;
		add(lblQuitGame, gbc_QuitGame);
		
		//Listener
		lblStartGame.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				SoundHandler.playSound("uiButtonClick", Integer.parseInt(Main.getOptions().getOption("sound.volume")));
				Main.getGameFrame().loadGame(Main.getOptions().getOption("game.selectedArena"));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblTopGreater.setVisible(true);
				lblTopLesser.setVisible(true);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblTopGreater.setVisible(false);
				lblTopLesser.setVisible(false);
			}
		});
		
		lblOptions.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				SoundHandler.playSound("uiButtonClick", Integer.parseInt(Main.getOptions().getOption("sound.volume")));
				Main.getGameFrame().loadOptionMenue();
				
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblMidGreater.setVisible(true);
				lblMidLesser.setVisible(true);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblMidGreater.setVisible(false);
				lblMidLesser.setVisible(false);
			}
		});
		
		lblQuitGame.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				try {
					Main.getOptions().saveOptions();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				System.exit(0);
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblBotGreater.setVisible(true);
				lblBotLesser.setVisible(true);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblBotGreater.setVisible(false);
				lblBotLesser.setVisible(false);
			}
		});	
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.drawImage(backgroundImage, 0, yStartingValue, null);
		
	}
	
	public boolean getStartGame() {
		return startGame;
	}

}
