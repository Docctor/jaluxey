package view;

import javax.swing.JPanel;

import main.Main;
import system.ScreenSelection;
import system.SoundHandler;
import system.TextureHandler;

import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GameOptionsMenue extends JPanel {

	private Font titleFont = TextureHandler.getFont(0).deriveFont(80f);
	private Font labelFont = TextureHandler.getFont(0).deriveFont(30f);
	
	private Image backgroundImage = ScreenSelection.getInstance().resizeToScreenSize("GameOptionsMenue").getImage();
	
	private int width = ScreenSelection.getInstance().getScreenWidth(), height = ScreenSelection.getInstance().getScreenHeight(), yStartingValue = ScreenSelection.getInstance().getYStartingValue();

	/**
	 * Create the panel.
	 */
	public GameOptionsMenue() {
		
		setBounds(0, 0, width, height);
		setBackground(Color.BLACK);
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0};
		setLayout(gridBagLayout);
		
		JLabel lblTopGreater = new JLabel(">");
		
		lblTopGreater.setVisible(false);
		lblTopGreater.setFont(labelFont);
		lblTopGreater.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblTopGreater = new GridBagConstraints();
		gbc_lblTopGreater.insets = new Insets(0, 0, 25, 0);
		gbc_lblTopGreater.gridx = 0;
		gbc_lblTopGreater.gridy = 1;
		add(lblTopGreater, gbc_lblTopGreater);
		
		JLabel lblBotGreater = new JLabel(">");
		
		lblBotGreater.setVisible(false);
		lblBotGreater.setFont(labelFont);
		lblBotGreater.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblBotGreater = new GridBagConstraints();
		gbc_lblBotGreater.gridx = 0;
		gbc_lblBotGreater.gridy = 2;
		add(lblBotGreater, gbc_lblBotGreater);
		
		JLabel lblTopLesser = new JLabel("<");
		
		lblTopLesser.setVisible(false);
		lblTopLesser.setFont(labelFont);
		lblTopLesser.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblTopLesser = new GridBagConstraints();
		gbc_lblTopLesser.insets = new Insets(0, 0, 25, 0);
		gbc_lblTopLesser.gridx = 2;
		gbc_lblTopLesser.gridy = 1;
		add(lblTopLesser, gbc_lblTopLesser);
		
		JLabel lblBotLesser = new JLabel("<");
		
		lblBotLesser.setVisible(false);
		lblBotLesser.setFont(labelFont);
		lblBotLesser.setForeground(Color.WHITE);
		
		GridBagConstraints gbc_lblBotLesser = new GridBagConstraints();
		gbc_lblBotLesser.gridx = 2;
		gbc_lblBotLesser.gridy = 2;
		add(lblBotLesser, gbc_lblBotLesser);
		
		JLabel lblGame = new JLabel("Game");
		
		lblGame.setForeground(Color.WHITE);
		lblGame.setFont(labelFont);
		
		GridBagConstraints gbc_lblGame = new GridBagConstraints();
		gbc_lblGame.insets = new Insets(0, 0, 25, 0);
		gbc_lblGame.gridx = 1;
		gbc_lblGame.gridy = 1;
		add(lblGame, gbc_lblGame);
		
		JLabel lblAudio = new JLabel("Audio");
		
		lblAudio.setOpaque(false);
		lblAudio.setForeground(Color.WHITE);
		lblAudio.setFont(labelFont);
		
		GridBagConstraints gbc_lblAudio = new GridBagConstraints();
		gbc_lblAudio.gridx = 1;
		gbc_lblAudio.gridy = 2;
		add(lblAudio, gbc_lblAudio);

		Main.getGameFrame().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if ( isVisible() && e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					Main.getGameFrame().loadMainMenu();
				}
			}
		});
		
		lblGame.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				SoundHandler.playSound("uiButtonClick", Integer.parseInt(Main.getOptions().getOption("sound.volume")));
				Main.getGameFrame().loadOptionGameMenue();
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblTopGreater.setVisible(true);
				lblTopLesser.setVisible(true);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblTopGreater.setVisible(false);
				lblTopLesser.setVisible(false);
			}
		});
		
		lblAudio.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				SoundHandler.playSound("uiButtonClick", Integer.parseInt(Main.getOptions().getOption("sound.volume")));
				Main.getGameFrame().loadOptionAudioMenue();
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblBotGreater.setVisible(true);
				lblBotLesser.setVisible(true);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblBotGreater.setVisible(false);
				lblBotLesser.setVisible(false);
			}
		});
		
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(backgroundImage, 0, yStartingValue, null);
	}
}
