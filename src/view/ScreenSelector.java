package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import system.ScreenSelection;

@SuppressWarnings("serial")
public class ScreenSelector extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public ScreenSelector() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();
		setBounds((int)(width * 0.25), (int)(height * 0.25), (int)(width * 0.5), (int)(height * 0.5));
		setTitle("JaLuxEy");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setUndecorated(true);

		contentPane = new JPanel();
		setContentPane(contentPane);

		loadScreenSelector();

	}

	public void loadScreenSelector () {
		// Contentpane neu initialisieren, um sie nutzen zu k�nnen
		contentPane = new JPanel();
		setContentPane(contentPane);
		setBackground(Color.WHITE);
		contentPane.setLayout(new BorderLayout(0, 0));

		// Bilder im Array zwischen Speichern
		BufferedImage[] screenImages = new BufferedImage[ScreenSelection.getInstance().getScreenNumber()];
		for (int i = 0; i < screenImages.length; i++) {
			screenImages[i] = ScreenSelection.getInstance().takeScreenshot(i);
		}

		// Erstellung des Infotextes
		JLabel lblChooseInfo = new JLabel("W�hlen Sie bitte Ihren Bildschirm");
		lblChooseInfo.setBackground(null);
		lblChooseInfo.setBorder(null);
		lblChooseInfo.setFont(new Font("Plain", Font.BOLD, 16));
		contentPane.add(lblChooseInfo, BorderLayout.NORTH);

		// Erstellung der Scrollpane und Initialisierung der Werte des GridBadLayouts f�r die Screenliste
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.getVerticalScrollBar().setUnitIncrement(15);
		contentPane.add(scrollPane, BorderLayout.CENTER);

		JPanel pnlScreenList = new JPanel();
		scrollPane.setViewportView(pnlScreenList);

		GridBagLayout gbl_screenList = new GridBagLayout();
		int[] rows = new int[Math.round(screenImages.length / 2)];
		double[] weight = new double[Math.round(screenImages.length / 2)];

		for (int i = 0; i < rows.length; i++) {
			rows[i] = 0;
			weight[i] = 0;
		}

		gbl_screenList.columnWidths = new int[]{0, 0};
		gbl_screenList.rowHeights = rows;
		gbl_screenList.columnWeights = new double[]{1.0, 1.0};
		gbl_screenList.rowWeights = weight;
		pnlScreenList.setLayout(gbl_screenList);

		// Erstellung des Scrollpane Innenlebens, also die Bildschirmscreenshots mit jeweiliger Beschriftung
		for (int i = 0; i < screenImages.length; i++) {

			JPanel pnlScreenListText = new JPanel();
			pnlScreenListText.setLayout(new BorderLayout(0, 0));

			GridBagConstraints constraints = new GridBagConstraints();
			if (i % 2 != 0) {
				constraints.gridx = 1;
			} else {
			constraints.gridx = 0;
			}
			constraints.gridy = i/2;
			pnlScreenList.add(pnlScreenListText, constraints);

			JLabel lblScreenImageText = new JLabel("Bildschirm " + (i+1));
			pnlScreenListText.add(lblScreenImageText, BorderLayout.NORTH);

			JLabel lblScreenImage = new JLabel();
			Image newScreenImage = new ImageIcon(screenImages[i]).getImage().getScaledInstance(1920 / 5, 1080 / 5, java.awt.Image.SCALE_SMOOTH);
			lblScreenImage.setIcon(new ImageIcon(newScreenImage));
			pnlScreenListText.add(lblScreenImage, BorderLayout.CENTER);

			//Hole die Screennummer und wechsel den Screen
			lblScreenImage.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					ScreenSelection.getInstance().setScreenDigit(Integer.parseInt(lblScreenImageText.getText().replaceAll("[^0-9]",""))-1);
				}
			});
		}

		revalidate();
		repaint();
	}

}
