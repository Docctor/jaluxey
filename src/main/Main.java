package main;

import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import game.GameLoop;
import git.bMINUSdocctor.ioStream.UnkownIOMode;
import git.bMINUSdocctor.ioStream.WrongIOMode;
import system.Arena;
import system.OptionsHandler;
import system.ScreenSelection;
import system.SoundHandler;
import system.TextureHandler;
import view.GameFrameGUI;
import view.ScreenSelector;

public class Main {

	private static GameLoop gameLoop;
	private static GameFrameGUI gameFrame;
	private static OptionsHandler options;
	
	//START SETTINGS
	private static int minimumScroll = 1, maximumScroll = 5, offSetX = 0, offSetY = 0, cellPixelSize = 50;
	private static float scaleIntensity = 0.1F, scaleX = 1.0F, scaleY = 1.0F;
	//END OF STARTSETTINGS
	
	public static void main(String[] args) {
		//SCREENSELECTION
		ScreenSelector screenSelection = new ScreenSelector();
		screenSelection.setVisible(true);
		while (ScreenSelection.getInstance().getScreenDigit() == -1) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {}
		}
		screenSelection.setVisible(false);
		screenSelection.removeAll();
		screenSelection = null;
		//END OF SCREENSELECTION
		
		//LOADING THE CONFIG FILE
		options = new OptionsHandler();
		try {
			options.saveDefaultConfig();
			options.loadConfig();
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
		//END OF LOADING THE CONFIG FILE
		
		//LOAD SOUNDS
		try {
			SoundHandler.loadSounds();
		} catch (UnsupportedAudioFileException | IOException | IllegalStateException | LineUnavailableException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
		//END OF LOAD SOUNDS
		
		//LOAD MAPS
		try {
			Arena.loadArenas();
		} catch (IOException | UnkownIOMode | WrongIOMode e) {
			e.printStackTrace();
			System.exit(1);
		}
		//END OF LOAD MAPS
		
		//LOADS TEXTURES
		TextureHandler.loadImages();
		TextureHandler.loadFonts();
		//HIER HABE ICH ETWAS VER�NDERT, BILDER WERDEN JETZT NACH DEM ERSTELLEN DES GAME OBJEKTS GELADEN UND EVT. ERSTELLT
		TextureHandler.loadScrollStages();
		
		gameFrame = new GameFrameGUI(ScreenSelection.getInstance().getKeyPoint());
		gameFrame.loadMainMenu();
		gameFrame.setVisible(true);
		
		gameLoop = new GameLoop();
		gameLoop.createInputListener();
	}
	
	public static GameLoop getGameLoop() {
		return gameLoop;
	}
	
	public static void setGameLoop(GameLoop gameLoop) {
		Main.gameLoop = gameLoop;
	}
	
	public static GameFrameGUI getGameFrame() {
		return gameFrame;
	}
	
	public static int getMinimumScroll() {
		return minimumScroll;
	}
	
	public static int getMaximumScroll() {
		return maximumScroll;
	}
	
	public static float getScaleIntensity() {
		return scaleIntensity;
	}

	public static int getOffSetX() {
		return offSetX;
	}

	public static void setOffSetX(int offSetX) {
		Main.offSetX = offSetX;
	}

	public static int getOffSetY() {
		return offSetY;
	}

	public static void setOffSetY(int offSetY) {
		Main.offSetY = offSetY;
	}

	public static float getScaleX() {
		return scaleX;
	}

	public static void setScaleX(float scaleX) {
		Main.scaleX = scaleX;
	}

	public static float getScaleY() {
		return scaleY;
	}

	public static void setScaleY(float scaleY) {
		Main.scaleY = scaleY;
	}

	public static int getCellPixelSize() {
		return cellPixelSize;
	}

	public static void setCellPixelSize(int cellPixelSize) {
		Main.cellPixelSize = cellPixelSize;
	}
	
	public static OptionsHandler getOptions() {
		return options;
	}
}
