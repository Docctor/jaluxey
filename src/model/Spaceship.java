package model;

import java.awt.event.MouseEvent;
import java.util.Random;

import git.bMINUSdocctor.world2D.World2D;
import git.bMINUSdocctor.world2D.World2DObject;

public class Spaceship extends Actor {

	public Spaceship(double x, double y, int team) {
		this.x = x;
		this.y = y;
		this.team = team;
	}
	
	public void spaceshipTargetCommunication(MouseEvent e) {
		//TODO Es sollen mithilfe eines linken Mausklicks die Zielkoordinaten an movement geliefert werden
		double xTarget = e.getX();
		double yTarget = e.getY();
		//f�r jedes Spaceship im Auswahlrahmen:
		//for(World2DObject s : World2D.getObjectsInAuswahlrahmen()) {
			//movement(xTarget, yTarget);
		//}
		
	}
	
	public void spaceshipDistribution(double xTarget, double yTarget) {
		  Random randX = new Random();
		  Random randY = new Random();
		  double xDistribution = xTarget - 1.5 + (xTarget + 1.5 - xTarget - 1.5) * randX.nextDouble();
		  double yDistribution = yTarget - 1.5 + (yTarget + 1.5 - yTarget - 1.5) * randY.nextDouble();
		  double deltaX = xDistribution - this.x;
		  double deltaY = yDistribution - this.y;
		  double time = 1;
		  while(deltaX < 0.5 && deltaY < 0.5) {
			  CustomThread t1 = new CustomThread(this.x, this.y, xTarget, yTarget, deltaX, deltaY, time);
			  t1.start();
		  }
	}
	
	public void spaceshipDestruction() {
		//TODO Das Spaceship wird gel�scht
	}
	
	public void movement(double xTarget, double yTarget) {
		double deltaX = xTarget - this.x;
		double deltaY = yTarget - this.y;
		double time = 1;
		//Distanz zum Zielpunkt, bei der die Raumschiffe verteilt werden sollen
		while(deltaX < 0.5 && deltaY < 0.5) {
			CustomThread t1 = new CustomThread(this.x, this.y, xTarget, yTarget, deltaX, deltaY, time);
			t1.start();
		}
		this.spaceshipDistribution(xTarget, yTarget);
	}
	
	public void spaceshipMarking() {
		/*TODO Spaceships, welche sich im Auswahlrahmen befinden,
		 * sollen zu einer Spaceshipliste geaddet werden, die Liste geht an spaceshipTargetCommunication() weiter
		 */
		
		
	}

	@Override
	public String toString() {
		return "Spaceship [team=" + team + ", x=" + x + ", y=" + y + "]";
	}
}
