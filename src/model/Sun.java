package model;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JLabel;

import git.bMINUSdocctor.world2D.World2DObject;
import team.TeamColor;

public class Sun {

	public static Map<String, Sun> allSuns = new HashMap<String, Sun>();
	
	private int lifePoints, level, levelCap, width, height;
	private JLabel component;
	private TeamColor team;
	private float x, y;
	private World2DObject object;
	private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	public Sun(float x, float y, TeamColor team, int level, int levelCap) {
		this.x = x;
		this.y = y;
		this.team = team;
		this.level = level;
		this.levelCap = levelCap;
	}

	public void sunConquest() {
		//TODO Siehe Aktivitätsdiagramm
		if(this.team == null && this.lifePoints == 0) {
			++this.lifePoints;
			if(lifePoints == 25) {
				//this.team = (team welchem die Lebenspunkte angehören)
			}
		} else {
			--this.lifePoints;
			if(this.lifePoints == 0) {
				this.team = null;
			}
		}
	}

	public void changeTeam(TeamColor team) {
		this.setTeam(team);
	}

	public void produceSpaceships() {
		Runnable spaceshipProducer = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				//this. nicht möglich
				if(level == 1) {
					//a spaceship gets produced
					//the spaceship calls spaceshipDistribution
				} else if(level == 2) {
					//two spaceships get produced
					//the spaceships call spaceshipDistribution
				} else if(level == 3) {
					//three spaceships get produced
					//the spaceships call spaceshipDistribution
				}
			}
		};
		//every second, the runnable gets executed
		scheduler.scheduleAtFixedRate(spaceshipProducer, 0, 1, TimeUnit.SECONDS);
	}
	
	

	public int getLifePoints() {
		return lifePoints;
	}

	public void setLifePoints(int lifePoints) {
		this.lifePoints = lifePoints;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getLevelCap() {
		return levelCap;
	}

	public void setLevelCap(int levelCap) {
		this.levelCap = levelCap;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public TeamColor getTeam() {
		return team;
	}

	public void setTeam(TeamColor team) {
		this.team = team;
	}

	public JLabel getComponent() {
		return component;
	}

	public void setComponent(JLabel component) {
		this.component = component;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
	public void setWorld2DObject(World2DObject object) {
		this.object = object;
	}
	
	public World2DObject getWorld2DObject() {
		return object;
	}
}
