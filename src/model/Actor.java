package model;

public abstract class Actor {
	
	int team;
	double x, y;

	public Actor() {}

	public double getX() {
		return Math.round(x);
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return Math.round(y);
	}

	public void setY(double y) {
		this.y = y;
	}

	public int getTeam() {
		return team;
	}

	public void setTeam(int team) {
		this.team = team;
	}
} //Unit wird sp�ter als Oberklasse f�r Spaceship dienen, change to interface?
