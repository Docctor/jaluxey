package model;

import java.awt.Image;
import java.awt.Window;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import game.KeyPoint;
import system.TextureHandler;

public class SpriteSheetAnimation {

	private ArrayList<ImageIcon> spriteFrames;
	
	
	private ImageIcon sprite;
	
	boolean running;
	
	private long speed, previousTime, time;
	
	private int frameAtPause, currentFrame, spriteWidth, spriteHeight;
	
	private String key;

	private JLabel animationComponent;
	
	
	public SpriteSheetAnimation(String key, long speed, int variant) {
		spriteFrames = new ArrayList<ImageIcon>();
		this.key = key;
		switch (variant) {
		case 1:
			getSprites(key);
			break;
		case 2:
			getPulsatingSprites(key);
			break;
		
		default:
			System.out.println("Your mom stinks");
			break;
		}
		this.speed = speed;
		this.animationComponent = new JLabel();
		spriteWidth = spriteFrames.get(currentFrame).getIconWidth();
		spriteHeight = spriteFrames.get(currentFrame).getIconHeight();
	}
	
	
	
	public void renderSpriteAnimation() {
		if (running) {
			time = System.currentTimeMillis();
			if (time - previousTime >= speed) {
				
				try {
					sprite = spriteFrames.get(currentFrame);
					currentFrame++;
					if (currentFrame >= spriteFrames.size()) {
						currentFrame = 0;
					}
				} catch (IndexOutOfBoundsException e) {
					currentFrame = 0;
					sprite = spriteFrames.get(currentFrame);
				}
				previousTime = time;
			}
		}
	}
	
	public void startAnimation() {
		running = true;
		frameAtPause = 0;
		currentFrame = 0;
		previousTime = 0;
	}

	public void stopAnimation() {
		running = false;
		frameAtPause = 0;
		currentFrame = 0;
		previousTime = 0;
	}
	
	public void pauseAnimation() {
		running = false;
		frameAtPause = currentFrame;
	}
	
	public void resumeAnimation() {
		running = true;
		currentFrame = frameAtPause;
	}
	
	//gerade unn�tig
	public void setSpeed(long speed) {
		this.speed = speed;
	}
	
	public void getSprites(String key) {

		for (int i = 0; true; i++) {
			if(TextureHandler.getImage(key + (i)) == null) {
				break;
			}
			spriteFrames.add(TextureHandler.getImage(key + (i)));
		}
	}
	
	public void getPulsatingSprites(String key) {
		if (spriteFrames.get(0) == null) {
			spriteFrames.add(TextureHandler.getImage(key));
		}	
		for (int i = 0; true; i++) {
			if (TextureHandler.getImage(key + "_pulsating" + (i)) == null) {
				for (int j = spriteFrames.size()-2; true; j--) {
					if (TextureHandler.getImage(key + "_pulsating" + (j)) == null) {
						break;
					}
					spriteFrames.add(TextureHandler.getImage(key + "_pulsating" + (j)));
				}
				break;
			}
			spriteFrames.add(TextureHandler.getImage(key + "_pulsating" + (i)));
		}
	}
	
	public ImageIcon getActualSprite() {
		return sprite;
	}

	public JLabel getAnimationComponent() {
		return animationComponent;
	}

	public void setAnimationComponent(JLabel animationComponent) {
		this.animationComponent = animationComponent;
	}
	

	public int getSpriteWidth() {
		return spriteWidth;
	}

	public void setSpriteWidth(int spriteWidth) {
		this.spriteWidth = spriteWidth;
	}

	public int getSpriteHeight() {
		return spriteHeight;
	}

	public void setSpriteHeight(int spriteHeight) {
		this.spriteHeight = spriteHeight;
	}
	
	public ArrayList<ImageIcon> getSpriteFrames() {
		return spriteFrames;
	}

	public int getCurrentFrame() {
		return currentFrame;
	}
	
	public String getSpriteKey() {
		return key;
	}
}
