package model;

public class CustomThread extends Thread{

	private double deltaY;
	private double time;
	private double x;
	private double y;
	private double xTarget;
	private double yTarget;
	private double deltaX;
	
	public CustomThread(double x, double y, double xTarget, double yTarget, double deltaX, double deltaY, double time) {
		this.deltaY = deltaY;
		this.time = time;
		this.x = x;
		this.y = y;
		this.xTarget = xTarget;
		this.yTarget = yTarget;
		this.deltaX = deltaX;
	}



	@Override
	public void run() {
		// TODO Auto-generated method stub
	    long startTime = System.nanoTime();
	    deltaX = xTarget - x;
	    deltaY = yTarget - y;
	    double angle = Math.atan2(deltaY, deltaX);
	    x += time * Math.cos(angle);
	    y += time * Math.sin(angle);
	    long endTime = System.nanoTime();
	    //Um die Geschwindigkeit zu erh�hen, muss die Zeit mit einer weiteren Konstante dividiert werden
	    time = (endTime - startTime) / 1e9;
	}

}
