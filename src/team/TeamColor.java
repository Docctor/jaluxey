/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package team;

public enum TeamColor {
	ORANGE,
	BLUE,
	GREEN,
	NONE
}
