/*
* Copyright (C) 2021, Kilian David Wos (Aliases: Docctor; GitHub: B-Docctor)
*/
package team;

import java.util.ArrayList;
import java.util.List;

import model.Spaceship;
import model.Sun;

public class Team {
	private TeamColor color;
	private List<Sun> suns;
	private List<Spaceship> units;
	
	
	public Team(TeamColor color) {
		this.color = color;
		suns = new ArrayList<Sun>();
		units = new ArrayList<Spaceship>();
	}
	
	public TeamColor getTeamColor() {
		return color;
	}
	
	public List<Sun> getSuns() {
		return suns;
	}
	
	public List<Spaceship> getUnits() {
		return units;
	}
}
